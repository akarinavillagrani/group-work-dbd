insert into tipo_usuario (descripcion) values ('Persona Natural');
insert into tipo_usuario (descripcion) values ('Persona Juridica');

insert into TIPO_TRANSACCION (descripcion) values ('ABONO');
insert into TIPO_TRANSACCION (descripcion) values ('CARGO');

insert into USUARIO
(rut,nombre,apellido,email,direccion,telefono,sexo,es_activo,es_verificado,nacionalidad, fecha_creacion_usu, nro_serie, carnet_foto, tipo_usuario)
	values
('11462052-1'	, 'Empresa AB'	, 'N/A'		,'gerencia@AB.com'		,'DIRECCIÓN 1'	,984718961	, 'N/A'	,true,true	,'Chile'		,'01-01-2018'	,'1234567'	,B'10'::bit(1000),2),
('21547343-0'	, 'Empresa CD'	, 'N/A'		,'inversiones@AB.com'	,'DIRECCIÓN 2'	,884728962	, 'N/A'	,false,false,'Chile'		,'01-02-2019'	,'9923112'	,B'10'::bit(1000),2),
('11509611-7'	, 'Empresa EF'	, 'N/A'		,'contacto@AB.com'		,'DIRECCIÓN 3'	,784738963	, 'N/A'	,true,true	,'Chile'		,'01-03-2018'	,'8934113'	,B'10'::bit(1000),2),
('22481678-2'	, 'Empresa FG'	, 'N/A'		,'gerencia@FG.com'		,'DIRECCIÓN 4'	,984748964	, 'N/A'	,true,true	,'Chile'		,'01-04-2017'	,'7945114'	,B'10'::bit(1000),2),
('6067897-9'	, 'Empresa HI'	, 'N/A'		,'bolsa@HI.com'			,'DIRECCIÓN 5'	,884758965	, 'N/A'	,true,true	,'Chile'		,'01-05-2017'	,'7967115'	,B'10'::bit(1000),2),
('13321389-9'	, 'Empresa JK'	, 'N/A'		,'inversiones@JK.com'	,'DIRECCIÓN 6'	,784768966	, 'N/A'	,true,true	,'Chile'		,'01-06-2018'	,'6988116'	,B'10'::bit(1000),2),
('16247535-5'	, 'Empresa LM'	, 'N/A'		,'gerencia@LM.com'		,'DIRECCIÓN 7'	,984778967	, 'N/A'	,true,true	,'Chile'		,'01-07-2019'	,'5966117'	,B'10'::bit(1000),2),
('17226690-8'	, 'Empresa NÑ'	, 'N/A'		,'asistencia@NN.com'	,'DIRECCIÓN 8'	,884788968	, 'N/A'	,true,true	,'Chile'		,'01-08-2018'	,'4967118'	,B'10'::bit(1000),2),
('10476085-6'	, 'Empresa OP'	, 'N/A'		,'contacto@OP.com'		,'DIRECCIÓN 9'	,784798969	, 'N/A'	,true,true	,'Chile'		,'01-09-2017'	,'3933119'	,B'10'::bit(1000),2),
('8412013-8'	, 'Empresa QR'	, 'N/A'		,'contacto@QR.com'		,'DIRECCIÓN 10'	,984708961	, 'N/A'	,true,true	,'Chile'		,'01-10-2018'	,'2934121'	,B'10'::bit(1000),2),
('10185418-3'	, 'Juan'		,'Perez'	,'gerencia@7c.com'		,'DIRECCIÓN 11'	,884718962	, 'N/A'	,true,true	,'Chile'		,'01-11-2014'	,'1966131'	,B'10'::bit(1000),1),
('9397378-k'	, 'Pedro'		,'Alarcon'	,'gerencia@8d.com'		,'DIRECCIÓN 12'	,784728963	, 'M'	,true,true	,'Chile'		,'01-12-2015'	,'0988141'	,B'10'::bit(1000),1),
('12296627-5'	, 'Pablo'		,'Conejeros','gerencia@9g.com'		,'DIRECCIÓN 13'	,994738964	, 'M'	,true,true	,'Chile'		,'01-01-2016'	,'9999151'	,B'10'::bit(1000),1),
('21417023-k'	, 'Alfredo'		,'Cortes'	,'gerencia@8h.com'		,'DIRECCIÓN 14'	,884458965	, 'M'	,true,true	,'Chile'		,'01-02-2017'	,'8955161'	,B'10'::bit(1000),1),
('16640642-0'	, 'Carlos'		,'Gajardo'	,'gerencia@7y.com'		,'DIRECCIÓN 15'	,974758966	, 'M'	,true,true	,'Chile'		,'01-03-2018'	,'7944171'	,B'10'::bit(1000),1),
('11384032-3'	, 'Natalia'		,'Gomez'	,'gerencia@6t.com'		,'DIRECCIÓN 16'	,994768967	, 'F'	,true,false	,'Chile'		,'01-04-2019'	,'6988181'	,B'10'::bit(1000),1),
('7638833-4'	, 'Jose'		,'Henriquez','gerencia@6e.com'		,'DIRECCIÓN 17'	,984778968	, 'M'	,true,true	,'Chile'		,'01-04-2012'	,'5945191'	,B'10'::bit(1000),1),
('5131281-3'	, 'Mauricio'	,'Lira'		,'gerencia@5r.com'		,'DIRECCIÓN 18'	,974788969	, 'M'	,true,true	,'Chile'		,'01-05-2013'	,'4913611'	,B'10'::bit(1000),1),
('19930243-4'	, 'Braulio'		,'Miranda'	,'bm@4t.com'			,'DIRECCIÓN 19'	,994798960	, 'M'	,false,false,'España'		,'01-06-2014'	,'3913712'	,B'10'::bit(1000),1),
('21296593-6'	, 'German'		,'Moreno'	,'gmoreno@4y.com'		,'DIRECCIÓN 20'	,984718961	, 'M'	,true,true	,'Paraguay'		,'01-07-2015'	,'2913813'	,B'10'::bit(1000),1),
('24812644-2'	, 'Rodrigo'		,'Muñoz'	,'rm@3u.com'			,'DIRECCIÓN 21'	,974728962	, 'M'	,true,true	,'Peru'			,'01-08-2016'	,'1913914'	,B'10'::bit(1000),1),
('19263854-2'	, 'Ana'			,'Reyes'	,'ana@3j.com'			,'DIRECCIÓN 22'	,994738963	, 'F'	,true,true	,'Colombia'		,'01-10-2017'	,'9914015'	,B'10'::bit(1000),1),
('13103822-4'	, 'Hector'		,'Toro'		,'htoro@1a.com'			,'DIRECCIÓN 23'	,984748964	, 'M'	,true,true	,'Chile'		,'01-11-2018'	,'8914116'	,B'10'::bit(1000),1),
('16026903-0'	, 'Juana'		,'Toledo'	,'jtolero@13a.com'		,'DIRECCIÓN 24'	,974758965	, 'F'	,false,true	,'Chile'		,'01-12-2019'	,'7914117'	,B'10'::bit(1000),1),
('11048524-7'	, 'Alberto'		,'Perez'	,'alp@as12.com'			,'DIRECCIÓN 25'	,994768966	, 'M'	,true,true	,'Argentina'	,'01-01-2020'	,'6914318'	,B'10'::bit(1000),1),
('22810378-0'	, 'Paula'		,'Rivas'	,'pp@pp.com'			,'DIRECCIÓN 26'	,977579671	, 'M'	,true,true	,'Chile'		,'01-02-2014'	,'5914419'	,B'10'::bit(1000),1),
('21517996-6'	, 'Paulina'		,'Pereira'	,'p.pereira@12AB.com'	,'DIRECCIÓN 27'	,994788968	, 'F'	,true,true	,'Chile'		,'01-03-2015'	,'4914515'	,B'10'::bit(1000),1),
('5602992-3'	,  'Luis'		,'Gonzalez'	,'luis@lvv.com'			,'DIRECCIÓN 28'	,984798969	, 'M'	,true,true	,'Chile'		,'01-04-2016'	,'3914661'	,B'10'::bit(1000),1),
('15730821-1'	, 'Carolina'	,'Cordova'	,'caina.cordova@cc.com'	,'DIRECCIÓN 29'	,994708960	, 'F'	,true,true	,'Chile'		,'01-05-2017'	,'2910471'	,B'10'::bit(1000),1),
('5538372-3'	, 'Ignacia'		,'Muñoz'	,'imuñoz@hila.com'		,'DIRECCIÓN 30'	,984718961	, 'F'	,true,true	,'Chile'		,'01-06-2018'	,'1910181'	,B'10'::bit(1000),1);


insert into empresa (rut ,razon_social ,nombre_fantasia ) values (12258402	,'Aesgener'							,'AASG') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (17311688	,'Vina Concha Toro'					,'CYT') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (22774293	,'Aguas Andinas'					,'AANDINAS') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (15310770	,'Banco De Chile (SN)'				,'BANCHILE') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (8856731	,'Banco de Credito e Inversiones'	,'BCI') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (7150842	,'Cap'								,'CAP') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (5411783	,'Cencosud'							,'CEONCOSUD2') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (20858697	,'Cencosud Shopping'				,'CENCOSUD') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (5758261	,'Cervecerias Unidas'				,'CCU') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (20249970	,'Colbun'							,'COLBUM') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (23113633	,'Embotelladora Andina'				,'ANDINA') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (11484173	,'Empresas CMPC'					,'CPMC') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (24168587	,'Empresas Copec'					,'COPEEC') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (8855518	,'ENEL Americas'					,'ENEL	') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (7806964	,'Enersis Chile'					,'ENERSIS') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (5949853	,'Engie Energia Chile'				,'EEC') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (6441360	,'Entel'							,'ENTEL') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (13923057	,'Falabella'						,'FALABELLA') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (10722726	,'Grupo Security'					,'SECURITY') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (21314552	,'Iam Sa'							,'QAS') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (14109490	,'Inv La Constru'					,'INV') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (17947662	,'Itau CorpBanca'					,'ITAU') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (10363746	,'Parq Arauco'						,'PARAUCO') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (14759921	,'Plaza	'							,'PLAZA') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (6963497	,'Ripley Corp'						,'RIPLEY');
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (19757297	,'Santander Chile'					,'SANTANDER') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (9442355	,'SMU'								,'SMU') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (21422702	,'Sonda'							,'SONDA') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (21690870	,'Soquimich B'						,'SQM') ;
insert into empresa (rut ,razon_social ,nombre_fantasia ) values (13858632	,'Vapores'							,'VAPO'); 

insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1,'AASG1','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2,'AASG2','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (3,'AASG3','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (4,'AASG4','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (5,'AASG5','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (6,'AASG6','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (7,'AASG7','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (8,'AASG8','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (9,'AASG9','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (10,'AASG10','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (11,'AASG11','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (12,'AASG12','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (13,'AASG13','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (14,'AASG14','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (15,'AASG15','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (16,'AASG16','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (17,'AASG17','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (18,'AASG18','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (19,'AASG19','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (20,'AASG20','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (21,'AASG21','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (22,'AASG22','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (23,'AASG23','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (24,'AASG24','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (25,'AASG25','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (26,'AASG26','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (27,'AASG27','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (28,'AASG28','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (29,'AASG29','01-01-2020',12258402);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (30,'AASG30','01-01-2020',12258402);

insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (31,'CYT31','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (32,'CYT32','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (33,'CYT33','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (34,'CYT34','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (35,'CYT35','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (36,'CYT36','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (37,'CYT37','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (38,'CYT38','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (39,'CYT39','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (40,'CYT40','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (41,'CYT41','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (42,'CYT42','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (43,'CYT43','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (44,'CYT44','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (45,'CYT45','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (46,'CYT46','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (47,'CYT47','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (48,'CYT48','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (49,'CYT49','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (50,'CYT50','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (51,'CYT51','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (52,'CYT52','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (53,'CYT53','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (54,'CYT54','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (55,'CYT55','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (56,'CYT56','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (57,'CYT57','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (58,'CYT58','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (59,'CYT59','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (60,'CYT60','01-10-2015',17311688);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (61,'AANDINAS61','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (62,'AANDINAS62','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (63,'AANDINAS63','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (64,'AANDINAS64','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (65,'AANDINAS65','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (66,'AANDINAS66','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (67,'AANDINAS67','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (68,'AANDINAS68','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (69,'AANDINAS69','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (70,'AANDINAS70','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (71,'AANDINAS71','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (72,'AANDINAS72','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (73,'AANDINAS73','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (74,'AANDINAS74','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (75,'AANDINAS75','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (76,'AANDINAS76','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (77,'AANDINAS77','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (78,'AANDINAS78','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (79,'AANDINAS79','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (80,'AANDINAS80','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (81,'AANDINAS81','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (82,'AANDINAS82','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (83,'AANDINAS83','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (84,'AANDINAS84','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (85,'AANDINAS85','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (86,'AANDINAS86','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (87,'AANDINAS87','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (88,'AANDINAS88','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (89,'AANDINAS89','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (90,'AANDINAS90','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (91,'AANDINAS91','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (92,'AANDINAS92','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (93,'AANDINAS93','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (94,'AANDINAS94','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (95,'AANDINAS95','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (96,'AANDINAS96','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (97,'AANDINAS97','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (98,'AANDINAS98','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (99,'AANDINAS99','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (100,'AANDINAS100','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (101,'AANDINAS101','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (102,'AANDINAS102','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (103,'AANDINAS103','01-10-2010',22774293);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (104,'BANCHILE104','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (105,'BANCHILE105','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (106,'BANCHILE106','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (107,'BANCHILE107','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (108,'BANCHILE108','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (109,'BANCHILE109','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (110,'BANCHILE110','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (111,'BANCHILE111','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (112,'BANCHILE112','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (113,'BANCHILE113','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (114,'BANCHILE114','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (115,'BANCHILE115','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (116,'BANCHILE116','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (117,'BANCHILE117','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (118,'BANCHILE118','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (119,'BANCHILE119','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (120,'BANCHILE120','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (121,'BANCHILE121','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (122,'BANCHILE122','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (123,'BANCHILE123','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (124,'BANCHILE124','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (125,'BANCHILE125','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (126,'BANCHILE126','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (127,'BANCHILE127','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (128,'BANCHILE128','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (129,'BANCHILE129','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (130,'BANCHILE130','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (131,'BANCHILE131','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (132,'BANCHILE132','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (133,'BANCHILE133','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (134,'BANCHILE134','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (135,'BANCHILE135','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (136,'BANCHILE136','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (137,'BANCHILE137','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (138,'BANCHILE138','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (139,'BANCHILE139','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (140,'BANCHILE140','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (141,'BANCHILE141','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (142,'BANCHILE142','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (143,'BANCHILE143','01-10-1980',15310770);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (144,'BCI144','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (145,'BCI145','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (146,'BCI146','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (147,'BCI147','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (148,'BCI148','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (149,'BCI149','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (150,'BCI150','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (151,'BCI151','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (152,'BCI152','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (153,'BCI153','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (154,'BCI154','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (155,'BCI155','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (156,'BCI156','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (157,'BCI157','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (158,'BCI158','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (159,'BCI159','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (160,'BCI160','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (161,'BCI161','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (162,'BCI162','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (163,'BCI163','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (164,'BCI164','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (165,'BCI165','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (166,'BCI166','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (167,'BCI167','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (168,'BCI168','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (169,'BCI169','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (170,'BCI170','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (171,'BCI171','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (172,'BCI172','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (173,'BCI173','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (174,'BCI174','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (175,'BCI175','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (176,'BCI176','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (177,'BCI177','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (178,'BCI178','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (179,'BCI179','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (180,'BCI180','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (181,'BCI181','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (182,'BCI182','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (183,'BCI183','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (184,'BCI184','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (185,'BCI185','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (186,'BCI186','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (187,'BCI187','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (188,'BCI188','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (189,'BCI189','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (190,'BCI190','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (191,'BCI191','01-05-2010',8856731);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (192,'CAP192','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (193,'CAP193','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (194,'CAP194','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (195,'CAP195','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (196,'CAP196','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (197,'CAP197','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (198,'CAP198','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (199,'CAP199','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (200,'CAP200','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (201,'CAP201','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (202,'CAP202','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (203,'CAP203','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (204,'CAP204','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (205,'CAP205','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (206,'CAP206','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (207,'CAP207','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (208,'CAP208','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (209,'CAP209','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (210,'CAP210','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (211,'CAP211','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (212,'CAP212','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (213,'CAP213','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (214,'CAP214','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (215,'CAP215','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (216,'CAP216','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (217,'CAP217','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (218,'CAP218','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (219,'CAP219','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (220,'CAP220','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (221,'CAP221','15-01-1998',7150842);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (222,'CEONCOSUD2222','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (223,'CEONCOSUD3223','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (224,'CEONCOSUD4224','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (225,'CEONCOSUD5225','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (226,'CEONCOSUD6226','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (227,'CEONCOSUD7227','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (228,'CEONCOSUD8228','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (229,'CEONCOSUD9229','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (230,'CEONCOSUD10230','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (231,'CEONCOSUD11231','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (232,'CEONCOSUD12232','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (233,'CEONCOSUD13233','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (234,'CEONCOSUD14234','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (235,'CEONCOSUD15235','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (236,'CEONCOSUD16236','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (237,'CEONCOSUD17237','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (238,'CEONCOSUD18238','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (239,'CEONCOSUD19239','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (240,'CEONCOSUD20240','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (241,'CEONCOSUD21241','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (242,'CEONCOSUD22242','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (243,'CEONCOSUD23243','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (244,'CEONCOSUD24244','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (245,'CEONCOSUD25245','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (246,'CEONCOSUD26246','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (247,'CEONCOSUD27247','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (248,'CEONCOSUD28248','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (249,'CEONCOSUD29249','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (250,'CEONCOSUD30250','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (251,'CEONCOSUD31251','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (252,'CEONCOSUD32252','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (253,'CEONCOSUD33253','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (254,'CEONCOSUD34254','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (255,'CEONCOSUD35255','01-08-2000',5411783);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (256,'CENCOSUD256','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (257,'CENCOSUD257','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (258,'CENCOSUD258','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (259,'CENCOSUD259','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (260,'CENCOSUD260','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (261,'CENCOSUD261','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (262,'CENCOSUD262','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (263,'CENCOSUD263','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (264,'CENCOSUD264','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (265,'CENCOSUD265','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (266,'CENCOSUD266','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (267,'CENCOSUD267','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (268,'CENCOSUD268','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (269,'CENCOSUD269','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (270,'CENCOSUD270','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (271,'CENCOSUD271','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (272,'CENCOSUD272','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (273,'CENCOSUD273','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (274,'CENCOSUD274','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (275,'CENCOSUD275','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (276,'CENCOSUD276','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (277,'CENCOSUD277','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (278,'CENCOSUD278','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (279,'CENCOSUD279','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (280,'CENCOSUD280','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (281,'CENCOSUD281','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (282,'CENCOSUD282','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (283,'CENCOSUD283','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (284,'CENCOSUD284','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (285,'CENCOSUD285','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (286,'CENCOSUD286','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (287,'CENCOSUD287','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (288,'CENCOSUD288','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (289,'CENCOSUD289','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (290,'CENCOSUD290','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (291,'CENCOSUD291','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (292,'CENCOSUD292','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (293,'CENCOSUD293','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (294,'CENCOSUD294','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (295,'CENCOSUD295','04-11-2006',20858697);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (296,'CCU257','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (297,'CCU258','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (298,'CCU259','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (299,'CCU260','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (300,'CCU261','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (301,'CCU262','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (302,'CCU263','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (303,'CCU264','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (304,'CCU265','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (305,'CCU266','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (306,'CCU267','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (307,'CCU268','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (308,'CCU269','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (309,'CCU270','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (310,'CCU271','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (311,'CCU272','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (312,'CCU273','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (313,'CCU274','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (314,'CCU275','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (315,'CCU276','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (316,'CCU277','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (317,'CCU278','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (318,'CCU279','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (319,'CCU280','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (320,'CCU281','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (321,'CCU282','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (322,'CCU283','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (323,'CCU284','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (324,'CCU285','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (325,'CCU286','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (326,'CCU287','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (327,'CCU288','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (328,'CCU289','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (329,'CCU290','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (330,'CCU291','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (331,'CCU292','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (332,'CCU293','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (333,'CCU294','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (334,'CCU295','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (335,'CCU296','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (336,'CCU297','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (337,'CCU298','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (338,'CCU299','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (339,'CCU300','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (340,'CCU301','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (341,'CCU302','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (342,'CCU303','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (343,'CCU304','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (344,'CCU305','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (345,'CCU306','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (346,'CCU307','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (347,'CCU308','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (348,'CCU309','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (349,'CCU310','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (350,'CCU311','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (351,'CCU312','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (352,'CCU313','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (353,'CCU314','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (354,'CCU315','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (355,'CCU316','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (356,'CCU317','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (357,'CCU318','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (358,'CCU319','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (359,'CCU320','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (360,'CCU321','15-03-1900',5758261);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (361,'COLBUM258','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (362,'COLBUM259','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (363,'COLBUM260','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (364,'COLBUM261','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (365,'COLBUM262','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (366,'COLBUM263','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (367,'COLBUM264','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (368,'COLBUM265','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (369,'COLBUM266','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (370,'COLBUM267','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (371,'COLBUM268','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (372,'COLBUM269','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (373,'COLBUM270','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (374,'COLBUM271','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (375,'COLBUM272','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (376,'COLBUM273','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (377,'COLBUM274','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (378,'COLBUM275','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (379,'COLBUM276','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (380,'COLBUM277','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (381,'COLBUM278','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (382,'COLBUM279','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (383,'COLBUM280','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (384,'COLBUM281','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (385,'COLBUM282','05-11-1985',20249970);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (386,'ANDINA259','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (387,'ANDINA260','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (388,'ANDINA261','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (389,'ANDINA262','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (390,'ANDINA263','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (391,'ANDINA264','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (392,'ANDINA265','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (393,'ANDINA266','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (394,'ANDINA267','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (395,'ANDINA268','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (396,'ANDINA269','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (397,'ANDINA270','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (398,'ANDINA271','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (399,'ANDINA272','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (400,'ANDINA273','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (401,'ANDINA274','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (402,'ANDINA275','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (403,'ANDINA276','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (404,'ANDINA277','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (405,'ANDINA278','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (406,'ANDINA279','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (407,'ANDINA280','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (408,'ANDINA281','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (409,'ANDINA282','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (410,'ANDINA283','11-02-1992',23113633);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (411,'CPMC260','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (412,'CPMC261','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (413,'CPMC262','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (414,'CPMC263','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (415,'CPMC264','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (416,'CPMC265','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (417,'CPMC266','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (418,'CPMC267','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (419,'CPMC268','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (420,'CPMC269','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (421,'CPMC270','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (422,'CPMC271','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (423,'CPMC272','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (424,'CPMC273','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (425,'CPMC274','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (426,'CPMC275','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (427,'CPMC276','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (428,'CPMC277','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (429,'CPMC278','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (430,'CPMC279','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (431,'CPMC280','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (432,'CPMC281','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (433,'CPMC282','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (434,'CPMC283','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (435,'CPMC284','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (436,'CPMC285','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (437,'CPMC286','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (438,'CPMC287','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (439,'CPMC288','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (440,'CPMC289','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (441,'CPMC290','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (442,'CPMC291','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (443,'CPMC292','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (444,'CPMC293','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (445,'CPMC294','14-03-1989',11484173);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (446,'COPEEC261','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (447,'COPEEC262','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (448,'COPEEC263','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (449,'COPEEC264','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (450,'COPEEC265','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (451,'COPEEC266','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (452,'COPEEC267','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (453,'COPEEC268','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (454,'COPEEC269','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (455,'COPEEC270','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (456,'COPEEC271','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (457,'COPEEC272','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (458,'COPEEC273','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (459,'COPEEC274','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (460,'COPEEC275','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (461,'COPEEC276','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (462,'COPEEC277','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (463,'COPEEC278','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (464,'COPEEC279','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (465,'COPEEC280','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (466,'COPEEC281','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (467,'COPEEC282','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (468,'COPEEC283','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (469,'COPEEC284','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (470,'COPEEC285','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (471,'COPEEC286','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (472,'COPEEC287','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (473,'COPEEC288','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (474,'COPEEC289','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (475,'COPEEC290','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (476,'COPEEC291','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (477,'COPEEC292','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (478,'COPEEC293','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (479,'COPEEC294','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (480,'COPEEC295','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (481,'COPEEC296','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (482,'COPEEC297','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (483,'COPEEC298','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (484,'COPEEC299','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (485,'COPEEC300','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (486,'COPEEC301','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (487,'COPEEC302','02-02-1998',24168587);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (488,'ENEL	262','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (489,'ENEL	263','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (490,'ENEL	264','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (491,'ENEL	265','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (492,'ENEL	266','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (493,'ENEL	267','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (494,'ENEL	268','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (495,'ENEL	269','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (496,'ENEL	270','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (497,'ENEL	271','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (498,'ENEL	272','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (499,'ENEL	273','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (500,'ENEL	274','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (501,'ENEL	275','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (502,'ENEL	276','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (503,'ENEL	277','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (504,'ENEL	278','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (505,'ENEL	279','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (506,'ENEL	280','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (507,'ENEL	281','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (508,'ENEL	282','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (509,'ENEL	283','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (510,'ENEL	284','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (511,'ENEL	285','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (512,'ENEL	286','03-03-1993',8855518);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (513,'ENERSIS263','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (514,'ENERSIS264','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (515,'ENERSIS265','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (516,'ENERSIS266','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (517,'ENERSIS267','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (518,'ENERSIS268','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (519,'ENERSIS269','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (520,'ENERSIS270','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (521,'ENERSIS271','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (522,'ENERSIS272','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (523,'ENERSIS273','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (524,'ENERSIS274','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (525,'ENERSIS275','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (526,'ENERSIS276','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (527,'ENERSIS277','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (528,'ENERSIS278','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (529,'ENERSIS279','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (530,'ENERSIS280','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (531,'ENERSIS281','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (532,'ENERSIS282','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (533,'ENERSIS283','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (534,'ENERSIS284','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (535,'ENERSIS285','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (536,'ENERSIS286','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (537,'ENERSIS287','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (538,'ENERSIS288','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (539,'ENERSIS289','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (540,'ENERSIS290','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (541,'ENERSIS291','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (542,'ENERSIS292','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (543,'ENERSIS293','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (544,'ENERSIS294','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (545,'ENERSIS295','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (546,'ENERSIS296','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (547,'ENERSIS297','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (548,'ENERSIS298','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (549,'ENERSIS299','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (550,'ENERSIS300','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (551,'ENERSIS301','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (552,'ENERSIS302','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (553,'ENERSIS303','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (554,'ENERSIS304','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (555,'ENERSIS305','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (556,'ENERSIS306','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (557,'ENERSIS307','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (558,'ENERSIS308','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (559,'ENERSIS309','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (560,'ENERSIS310','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (561,'ENERSIS311','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (562,'ENERSIS312','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (563,'ENERSIS313','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (564,'ENERSIS314','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (565,'ENERSIS315','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (566,'ENERSIS316','04-04-1992',7806964);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (567,'EEC264','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (568,'EEC265','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (569,'EEC266','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (570,'EEC267','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (571,'EEC268','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (572,'EEC269','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (573,'EEC270','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (574,'EEC271','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (575,'EEC272','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (576,'EEC273','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (577,'EEC274','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (578,'EEC275','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (579,'EEC276','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (580,'EEC277','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (581,'EEC278','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (582,'EEC279','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (583,'EEC280','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (584,'EEC281','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (585,'EEC282','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (586,'EEC283','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (587,'EEC284','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (588,'EEC285','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (589,'EEC286','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (590,'EEC287','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (591,'EEC288','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (592,'EEC289','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (593,'EEC290','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (594,'EEC291','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (595,'EEC292','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (596,'EEC293','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (597,'EEC294','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (598,'EEC295','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (599,'EEC296','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (600,'EEC297','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (601,'EEC298','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (602,'EEC299','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (603,'EEC300','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (604,'EEC301','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (605,'EEC302','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (606,'EEC303','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (607,'EEC304','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (608,'EEC305','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (609,'EEC306','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (610,'EEC307','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (611,'EEC308','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (612,'EEC309','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (613,'EEC310','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (614,'EEC311','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (615,'EEC312','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (616,'EEC313','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (617,'EEC314','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (618,'EEC315','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (619,'EEC316','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (620,'EEC317','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (621,'EEC318','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (622,'EEC319','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (623,'EEC320','05-05-1995',5949853);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (624,'ENTEL265','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (625,'ENTEL266','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (626,'ENTEL267','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (627,'ENTEL268','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (628,'ENTEL269','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (629,'ENTEL270','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (630,'ENTEL271','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (631,'ENTEL272','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (632,'ENTEL273','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (633,'ENTEL274','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (634,'ENTEL275','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (635,'ENTEL276','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (636,'ENTEL277','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (637,'ENTEL278','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (638,'ENTEL279','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (639,'ENTEL280','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (640,'ENTEL281','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (641,'ENTEL282','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (642,'ENTEL283','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (643,'ENTEL284','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (644,'ENTEL285','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (645,'ENTEL286','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (646,'ENTEL287','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (647,'ENTEL288','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (648,'ENTEL289','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (649,'ENTEL290','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (650,'ENTEL291','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (651,'ENTEL292','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (652,'ENTEL293','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (653,'ENTEL294','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (654,'ENTEL295','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (655,'ENTEL296','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (656,'ENTEL297','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (657,'ENTEL298','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (658,'ENTEL299','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (659,'ENTEL300','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (660,'ENTEL301','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (661,'ENTEL302','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (662,'ENTEL303','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (663,'ENTEL304','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (664,'ENTEL305','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (665,'ENTEL306','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (666,'ENTEL307','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (667,'ENTEL308','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (668,'ENTEL309','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (669,'ENTEL310','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (670,'ENTEL311','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (671,'ENTEL312','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (672,'ENTEL313','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (673,'ENTEL314','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (674,'ENTEL315','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (675,'ENTEL316','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (676,'ENTEL317','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (677,'ENTEL318','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (678,'ENTEL319','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (679,'ENTEL320','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (680,'ENTEL321','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (681,'ENTEL322','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (682,'ENTEL323','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (683,'ENTEL324','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (684,'ENTEL325','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (685,'ENTEL326','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (686,'ENTEL327','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (687,'ENTEL328','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (688,'ENTEL329','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (689,'ENTEL330','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (690,'ENTEL331','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (691,'ENTEL332','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (692,'ENTEL333','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (693,'ENTEL334','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (694,'ENTEL335','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (695,'ENTEL336','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (696,'ENTEL337','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (697,'ENTEL338','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (698,'ENTEL339','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (699,'ENTEL340','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (700,'ENTEL341','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (701,'ENTEL342','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (702,'ENTEL343','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (703,'ENTEL344','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (704,'ENTEL345','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (705,'ENTEL346','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (706,'ENTEL347','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (707,'ENTEL348','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (708,'ENTEL349','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (709,'ENTEL350','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (710,'ENTEL351','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (711,'ENTEL352','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (712,'ENTEL353','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (713,'ENTEL354','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (714,'ENTEL355','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (715,'ENTEL356','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (716,'ENTEL357','06-06-1986',6441360);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (717,'FALABELLA266','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (718,'FALABELLA267','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (719,'FALABELLA268','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (720,'FALABELLA269','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (721,'FALABELLA270','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (722,'FALABELLA271','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (723,'FALABELLA272','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (724,'FALABELLA273','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (725,'FALABELLA274','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (726,'FALABELLA275','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (727,'FALABELLA276','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (728,'FALABELLA277','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (729,'FALABELLA278','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (730,'FALABELLA279','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (731,'FALABELLA280','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (732,'FALABELLA281','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (733,'FALABELLA282','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (734,'FALABELLA283','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (735,'FALABELLA284','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (736,'FALABELLA285','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (737,'FALABELLA286','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (738,'FALABELLA287','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (739,'FALABELLA288','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (740,'FALABELLA289','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (741,'FALABELLA290','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (742,'FALABELLA291','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (743,'FALABELLA292','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (744,'FALABELLA293','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (745,'FALABELLA294','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (746,'FALABELLA295','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (747,'FALABELLA296','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (748,'FALABELLA297','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (749,'FALABELLA298','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (750,'FALABELLA299','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (751,'FALABELLA300','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (752,'FALABELLA301','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (753,'FALABELLA302','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (754,'FALABELLA303','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (755,'FALABELLA304','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (756,'FALABELLA305','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (757,'FALABELLA306','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (758,'FALABELLA307','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (759,'FALABELLA308','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (760,'FALABELLA309','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (761,'FALABELLA310','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (762,'FALABELLA311','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (763,'FALABELLA312','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (764,'FALABELLA313','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (765,'FALABELLA314','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (766,'FALABELLA315','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (767,'FALABELLA316','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (768,'FALABELLA317','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (769,'FALABELLA318','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (770,'FALABELLA319','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (771,'FALABELLA320','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (772,'FALABELLA321','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (773,'FALABELLA322','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (774,'FALABELLA323','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (775,'FALABELLA324','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (776,'FALABELLA325','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (777,'FALABELLA326','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (778,'FALABELLA327','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (779,'FALABELLA328','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (780,'FALABELLA329','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (781,'FALABELLA330','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (782,'FALABELLA331','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (783,'FALABELLA332','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (784,'FALABELLA333','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (785,'FALABELLA334','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (786,'FALABELLA335','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (787,'FALABELLA336','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (788,'FALABELLA337','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (789,'FALABELLA338','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (790,'FALABELLA339','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (791,'FALABELLA340','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (792,'FALABELLA341','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (793,'FALABELLA342','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (794,'FALABELLA343','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (795,'FALABELLA344','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (796,'FALABELLA345','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (797,'FALABELLA346','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (798,'FALABELLA347','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (799,'FALABELLA348','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (800,'FALABELLA349','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (801,'FALABELLA350','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (802,'FALABELLA351','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (803,'FALABELLA352','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (804,'FALABELLA353','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (805,'FALABELLA354','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (806,'FALABELLA355','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (807,'FALABELLA356','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (808,'FALABELLA357','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (809,'FALABELLA358','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (810,'FALABELLA359','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (811,'FALABELLA360','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (812,'FALABELLA361','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (813,'FALABELLA362','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (814,'FALABELLA363','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (815,'FALABELLA364','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (816,'FALABELLA365','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (817,'FALABELLA366','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (818,'FALABELLA367','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (819,'FALABELLA368','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (820,'FALABELLA369','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (821,'FALABELLA370','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (822,'FALABELLA371','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (823,'FALABELLA372','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (824,'FALABELLA373','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (825,'FALABELLA374','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (826,'FALABELLA375','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (827,'FALABELLA376','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (828,'FALABELLA377','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (829,'FALABELLA378','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (830,'FALABELLA379','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (831,'FALABELLA380','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (832,'FALABELLA381','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (833,'FALABELLA382','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (834,'FALABELLA383','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (835,'FALABELLA384','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (836,'FALABELLA385','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (837,'FALABELLA386','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (838,'FALABELLA387','07-07-1997',13923057);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (839,'SECURITY267','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (840,'SECURITY268','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (841,'SECURITY269','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (842,'SECURITY270','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (843,'SECURITY271','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (844,'SECURITY272','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (845,'SECURITY273','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (846,'SECURITY274','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (847,'SECURITY275','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (848,'SECURITY276','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (849,'SECURITY277','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (850,'SECURITY278','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (851,'SECURITY279','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (852,'SECURITY280','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (853,'SECURITY281','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (854,'SECURITY282','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (855,'SECURITY283','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (856,'SECURITY284','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (857,'SECURITY285','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (858,'SECURITY286','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (859,'SECURITY287','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (860,'SECURITY288','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (861,'SECURITY289','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (862,'SECURITY290','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (863,'SECURITY291','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (864,'SECURITY292','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (865,'SECURITY293','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (866,'SECURITY294','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (867,'SECURITY295','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (868,'SECURITY296','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (869,'SECURITY297','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (870,'SECURITY298','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (871,'SECURITY299','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (872,'SECURITY300','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (873,'SECURITY301','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (874,'SECURITY302','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (875,'SECURITY303','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (876,'SECURITY304','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (877,'SECURITY305','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (878,'SECURITY306','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (879,'SECURITY307','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (880,'SECURITY308','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (881,'SECURITY309','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (882,'SECURITY310','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (883,'SECURITY311','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (884,'SECURITY312','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (885,'SECURITY313','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (886,'SECURITY314','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (887,'SECURITY315','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (888,'SECURITY316','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (889,'SECURITY317','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (890,'SECURITY318','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (891,'SECURITY319','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (892,'SECURITY320','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (893,'SECURITY321','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (894,'SECURITY322','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (895,'SECURITY323','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (896,'SECURITY324','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (897,'SECURITY325','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (898,'SECURITY326','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (899,'SECURITY327','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (900,'SECURITY328','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (901,'SECURITY329','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (902,'SECURITY330','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (903,'SECURITY331','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (904,'SECURITY332','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (905,'SECURITY333','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (906,'SECURITY334','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (907,'SECURITY335','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (908,'SECURITY336','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (909,'SECURITY337','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (910,'SECURITY338','08-08-1998',10722726);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (911,'QAS268','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (912,'QAS269','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (913,'QAS270','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (914,'QAS271','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (915,'QAS272','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (916,'QAS273','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (917,'QAS274','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (918,'QAS275','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (919,'QAS276','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (920,'QAS277','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (921,'QAS278','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (922,'QAS279','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (923,'QAS280','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (924,'QAS281','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (925,'QAS282','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (926,'QAS283','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (927,'QAS284','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (928,'QAS285','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (929,'QAS286','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (930,'QAS287','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (931,'QAS288','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (932,'QAS289','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (933,'QAS290','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (934,'QAS291','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (935,'QAS292','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (936,'QAS293','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (937,'QAS294','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (938,'QAS295','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (939,'QAS296','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (940,'QAS297','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (941,'QAS298','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (942,'QAS299','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (943,'QAS300','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (944,'QAS301','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (945,'QAS302','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (946,'QAS303','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (947,'QAS304','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (948,'QAS305','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (949,'QAS306','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (950,'QAS307','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (951,'QAS308','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (952,'QAS309','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (953,'QAS310','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (954,'QAS311','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (955,'QAS312','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (956,'QAS313','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (957,'QAS314','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (958,'QAS315','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (959,'QAS316','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (960,'QAS317','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (961,'QAS318','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (962,'QAS319','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (963,'QAS320','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (964,'QAS321','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (965,'QAS322','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (966,'QAS323','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (967,'QAS324','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (968,'QAS325','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (969,'QAS326','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (970,'QAS327','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (971,'QAS328','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (972,'QAS329','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (973,'QAS330','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (974,'QAS331','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (975,'QAS332','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (976,'QAS333','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (977,'QAS334','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (978,'QAS335','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (979,'QAS336','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (980,'QAS337','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (981,'QAS338','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (982,'QAS339','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (983,'QAS340','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (984,'QAS341','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (985,'QAS342','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (986,'QAS343','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (987,'QAS344','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (988,'QAS345','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (989,'QAS346','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (990,'QAS347','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (991,'QAS348','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (992,'QAS349','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (993,'QAS350','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (994,'QAS351','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (995,'QAS352','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (996,'QAS353','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (997,'QAS354','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (998,'QAS355','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (999,'QAS356','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1000,'QAS357','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1001,'QAS358','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1002,'QAS359','09-09-1999',21314552);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1003,'INV269','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1004,'INV270','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1005,'INV271','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1006,'INV272','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1007,'INV273','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1008,'INV274','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1009,'INV275','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1010,'INV276','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1011,'INV277','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1012,'INV278','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1013,'INV279','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1014,'INV280','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1015,'INV281','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1016,'INV282','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1017,'INV283','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1018,'INV284','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1019,'INV285','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1020,'INV286','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1021,'INV287','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1022,'INV288','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1023,'INV289','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1024,'INV290','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1025,'INV291','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1026,'INV292','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1027,'INV293','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1028,'INV294','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1029,'INV295','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1030,'INV296','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1031,'INV297','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1032,'INV298','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1033,'INV299','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1034,'INV300','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1035,'INV301','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1036,'INV302','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1037,'INV303','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1038,'INV304','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1039,'INV305','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1040,'INV306','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1041,'INV307','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1042,'INV308','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1043,'INV309','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1044,'INV310','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1045,'INV311','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1046,'INV312','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1047,'INV313','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1048,'INV314','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1049,'INV315','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1050,'INV316','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1051,'INV317','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1052,'INV318','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1053,'INV319','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1054,'INV320','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1055,'INV321','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1056,'INV322','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1057,'INV323','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1058,'INV324','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1059,'INV325','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1060,'INV326','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1061,'INV327','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1062,'INV328','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1063,'INV329','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1064,'INV330','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1065,'INV331','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1066,'INV332','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1067,'INV333','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1068,'INV334','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1069,'INV335','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1070,'INV336','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1071,'INV337','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1072,'INV338','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1073,'INV339','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1074,'INV340','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1075,'INV341','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1076,'INV342','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1077,'INV343','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1078,'INV344','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1079,'INV345','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1080,'INV346','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1081,'INV347','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1082,'INV348','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1083,'INV349','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1084,'INV350','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1085,'INV351','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1086,'INV352','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1087,'INV353','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1088,'INV354','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1089,'INV355','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1090,'INV356','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1091,'INV357','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1092,'INV358','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1093,'INV359','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1094,'INV360','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1095,'INV361','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1096,'INV362','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1097,'INV363','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1098,'INV364','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1099,'INV365','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1100,'INV366','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1101,'INV367','10-10-2000',14109490);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1102,'ITAU270','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1103,'ITAU271','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1104,'ITAU272','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1105,'ITAU273','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1106,'ITAU274','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1107,'ITAU275','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1108,'ITAU276','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1109,'ITAU277','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1110,'ITAU278','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1111,'ITAU279','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1112,'ITAU280','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1113,'ITAU281','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1114,'ITAU282','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1115,'ITAU283','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1116,'ITAU284','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1117,'ITAU285','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1118,'ITAU286','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1119,'ITAU287','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1120,'ITAU288','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1121,'ITAU289','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1122,'ITAU290','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1123,'ITAU291','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1124,'ITAU292','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1125,'ITAU293','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1126,'ITAU294','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1127,'ITAU295','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1128,'ITAU296','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1129,'ITAU297','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1130,'ITAU298','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1131,'ITAU299','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1132,'ITAU300','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1133,'ITAU301','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1134,'ITAU302','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1135,'ITAU303','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1136,'ITAU304','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1137,'ITAU305','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1138,'ITAU306','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1139,'ITAU307','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1140,'ITAU308','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1141,'ITAU309','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1142,'ITAU310','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1143,'ITAU311','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1144,'ITAU312','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1145,'ITAU313','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1146,'ITAU314','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1147,'ITAU315','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1148,'ITAU316','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1149,'ITAU317','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1150,'ITAU318','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1151,'ITAU319','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1152,'ITAU320','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1153,'ITAU321','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1154,'ITAU322','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1155,'ITAU323','11-11-1991',17947662);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1156,'PARAUCO271','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1157,'PARAUCO272','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1158,'PARAUCO273','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1159,'PARAUCO274','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1160,'PARAUCO275','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1161,'PARAUCO276','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1162,'PARAUCO277','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1163,'PARAUCO278','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1164,'PARAUCO279','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1165,'PARAUCO280','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1166,'PARAUCO281','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1167,'PARAUCO282','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1168,'PARAUCO283','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1169,'PARAUCO284','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1170,'PARAUCO285','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1171,'PARAUCO286','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1172,'PARAUCO287','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1173,'PARAUCO288','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1174,'PARAUCO289','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1175,'PARAUCO290','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1176,'PARAUCO291','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1177,'PARAUCO292','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1178,'PARAUCO293','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1179,'PARAUCO294','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1180,'PARAUCO295','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1181,'PARAUCO296','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1182,'PARAUCO297','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1183,'PARAUCO298','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1184,'PARAUCO299','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1185,'PARAUCO300','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1186,'PARAUCO301','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1187,'PARAUCO302','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1188,'PARAUCO303','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1189,'PARAUCO304','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1190,'PARAUCO305','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1191,'PARAUCO306','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1192,'PARAUCO307','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1193,'PARAUCO308','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1194,'PARAUCO309','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1195,'PARAUCO310','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1196,'PARAUCO311','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1197,'PARAUCO312','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1198,'PARAUCO313','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1199,'PARAUCO314','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1200,'PARAUCO315','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1201,'PARAUCO316','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1202,'PARAUCO317','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1203,'PARAUCO318','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1204,'PARAUCO319','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1205,'PARAUCO320','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1206,'PARAUCO321','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1207,'PARAUCO322','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1208,'PARAUCO323','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1209,'PARAUCO324','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1210,'PARAUCO325','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1211,'PARAUCO326','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1212,'PARAUCO327','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1213,'PARAUCO328','12-12-1992',10363746);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1214,'PLAZA272','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1215,'PLAZA273','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1216,'PLAZA274','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1217,'PLAZA275','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1218,'PLAZA276','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1219,'PLAZA277','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1220,'PLAZA278','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1221,'PLAZA279','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1222,'PLAZA280','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1223,'PLAZA281','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1224,'PLAZA282','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1225,'PLAZA283','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1226,'PLAZA284','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1227,'PLAZA285','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1228,'PLAZA286','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1229,'PLAZA287','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1230,'PLAZA288','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1231,'PLAZA289','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1232,'PLAZA290','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1233,'PLAZA291','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1234,'PLAZA292','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1235,'PLAZA293','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1236,'PLAZA294','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1237,'PLAZA295','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1238,'PLAZA296','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1239,'PLAZA297','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1240,'PLAZA298','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1241,'PLAZA299','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1242,'PLAZA300','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1243,'PLAZA301','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1244,'PLAZA302','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1245,'PLAZA303','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1246,'PLAZA304','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1247,'PLAZA305','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1248,'PLAZA306','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1249,'PLAZA307','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1250,'PLAZA308','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1251,'PLAZA309','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1252,'PLAZA310','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1253,'PLAZA311','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1254,'PLAZA312','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1255,'PLAZA313','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1256,'PLAZA314','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1257,'PLAZA315','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1258,'PLAZA316','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1259,'PLAZA317','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1260,'PLAZA318','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1261,'PLAZA319','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1262,'PLAZA320','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1263,'PLAZA321','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1264,'PLAZA322','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1265,'PLAZA323','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1266,'PLAZA324','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1267,'PLAZA325','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1268,'PLAZA326','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1269,'PLAZA327','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1270,'PLAZA328','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1271,'PLAZA329','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1272,'PLAZA330','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1273,'PLAZA331','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1274,'PLAZA332','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1275,'PLAZA333','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1276,'PLAZA334','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1277,'PLAZA335','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1278,'PLAZA336','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1279,'PLAZA337','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1280,'PLAZA338','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1281,'PLAZA339','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1282,'PLAZA340','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1283,'PLAZA341','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1284,'PLAZA342','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1285,'PLAZA343','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1286,'PLAZA344','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1287,'PLAZA345','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1288,'PLAZA346','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1289,'PLAZA347','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1290,'PLAZA348','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1291,'PLAZA349','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1292,'PLAZA350','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1293,'PLAZA351','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1294,'PLAZA352','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1295,'PLAZA353','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1296,'PLAZA354','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1297,'PLAZA355','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1298,'PLAZA356','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1299,'PLAZA357','01-01-1992',14759921);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1300,'RIPLEY273','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1301,'RIPLEY274','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1302,'RIPLEY275','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1303,'RIPLEY276','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1304,'RIPLEY277','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1305,'RIPLEY278','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1306,'RIPLEY279','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1307,'RIPLEY280','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1308,'RIPLEY281','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1309,'RIPLEY282','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1310,'RIPLEY283','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1311,'RIPLEY284','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1312,'RIPLEY285','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1313,'RIPLEY286','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1314,'RIPLEY287','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1315,'RIPLEY288','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1316,'RIPLEY289','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1317,'RIPLEY290','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1318,'RIPLEY291','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1319,'RIPLEY292','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1320,'RIPLEY293','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1321,'RIPLEY294','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1322,'RIPLEY295','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1323,'RIPLEY296','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1324,'RIPLEY297','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1325,'RIPLEY298','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1326,'RIPLEY299','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1327,'RIPLEY300','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1328,'RIPLEY301','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1329,'RIPLEY302','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1330,'RIPLEY303','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1331,'RIPLEY304','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1332,'RIPLEY305','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1333,'RIPLEY306','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1334,'RIPLEY307','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1335,'RIPLEY308','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1336,'RIPLEY309','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1337,'RIPLEY310','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1338,'RIPLEY311','02-02-1996',6963497);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1339,'SANTANDER274','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1340,'SANTANDER275','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1341,'SANTANDER276','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1342,'SANTANDER277','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1343,'SANTANDER278','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1344,'SANTANDER279','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1345,'SANTANDER280','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1346,'SANTANDER281','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1347,'SANTANDER282','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1348,'SANTANDER283','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1349,'SANTANDER284','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1350,'SANTANDER285','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1351,'SANTANDER286','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1352,'SANTANDER287','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1353,'SANTANDER288','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1354,'SANTANDER289','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1355,'SANTANDER290','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1356,'SANTANDER291','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1357,'SANTANDER292','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1358,'SANTANDER293','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1359,'SANTANDER294','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1360,'SANTANDER295','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1361,'SANTANDER296','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1362,'SANTANDER297','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1363,'SANTANDER298','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1364,'SANTANDER299','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1365,'SANTANDER300','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1366,'SANTANDER301','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1367,'SANTANDER302','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1368,'SANTANDER303','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1369,'SANTANDER304','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1370,'SANTANDER305','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1371,'SANTANDER306','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1372,'SANTANDER307','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1373,'SANTANDER308','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1374,'SANTANDER309','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1375,'SANTANDER310','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1376,'SANTANDER311','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1377,'SANTANDER312','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1378,'SANTANDER313','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1379,'SANTANDER314','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1380,'SANTANDER315','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1381,'SANTANDER316','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1382,'SANTANDER317','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1383,'SANTANDER318','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1384,'SANTANDER319','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1385,'SANTANDER320','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1386,'SANTANDER321','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1387,'SANTANDER322','01-10-1992',19757297);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1388,'SMU275','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1389,'SMU276','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1390,'SMU277','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1391,'SMU278','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1392,'SMU279','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1393,'SMU280','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1394,'SMU281','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1395,'SMU282','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1396,'SMU283','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1397,'SMU284','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1398,'SMU285','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1399,'SMU286','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1400,'SMU287','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1401,'SMU288','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1402,'SMU289','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1403,'SMU290','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1404,'SMU291','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1405,'SMU292','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1406,'SMU293','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1407,'SMU294','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1408,'SMU295','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1409,'SMU296','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1410,'SMU297','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1411,'SMU298','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1412,'SMU299','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1413,'SMU300','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1414,'SMU301','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1415,'SMU302','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1416,'SMU303','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1417,'SMU304','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1418,'SMU305','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1419,'SMU306','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1420,'SMU307','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1421,'SMU308','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1422,'SMU309','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1423,'SMU310','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1424,'SMU311','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1425,'SMU312','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1426,'SMU313','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1427,'SMU314','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1428,'SMU315','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1429,'SMU316','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1430,'SMU317','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1431,'SMU318','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1432,'SMU319','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1433,'SMU320','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1434,'SMU321','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1435,'SMU322','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1436,'SMU323','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1437,'SMU324','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1438,'SMU325','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1439,'SMU326','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1440,'SMU327','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1441,'SMU328','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1442,'SMU329','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1443,'SMU330','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1444,'SMU331','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1445,'SMU332','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1446,'SMU333','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1447,'SMU334','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1448,'SMU335','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1449,'SMU336','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1450,'SMU337','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1451,'SMU338','15-03-1996',9442355);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1452,'SONDA276','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1453,'SONDA277','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1454,'SONDA278','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1455,'SONDA279','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1456,'SONDA280','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1457,'SONDA281','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1458,'SONDA282','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1459,'SONDA283','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1460,'SONDA284','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1461,'SONDA285','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1462,'SONDA286','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1463,'SONDA287','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1464,'SONDA288','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1465,'SONDA289','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1466,'SONDA290','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1467,'SONDA291','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1468,'SONDA292','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1469,'SONDA293','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1470,'SONDA294','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1471,'SONDA295','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1472,'SONDA296','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1473,'SONDA297','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1474,'SONDA298','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1475,'SONDA299','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1476,'SONDA300','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1477,'SONDA301','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1478,'SONDA302','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1479,'SONDA303','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1480,'SONDA304','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1481,'SONDA305','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1482,'SONDA306','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1483,'SONDA307','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1484,'SONDA308','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1485,'SONDA309','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1486,'SONDA310','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1487,'SONDA311','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1488,'SONDA312','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1489,'SONDA313','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1490,'SONDA314','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1491,'SONDA315','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1492,'SONDA316','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1493,'SONDA317','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1494,'SONDA318','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1495,'SONDA319','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1496,'SONDA320','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1497,'SONDA321','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1498,'SONDA322','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1499,'SONDA323','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1500,'SONDA324','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1501,'SONDA325','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1502,'SONDA326','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1503,'SONDA327','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1504,'SONDA328','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1505,'SONDA329','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1506,'SONDA330','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1507,'SONDA331','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1508,'SONDA332','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1509,'SONDA333','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1510,'SONDA334','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1511,'SONDA335','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1512,'SONDA336','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1513,'SONDA337','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1514,'SONDA338','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1515,'SONDA339','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1516,'SONDA340','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1517,'SONDA341','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1518,'SONDA342','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1519,'SONDA343','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1520,'SONDA344','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1521,'SONDA345','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1522,'SONDA346','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1523,'SONDA347','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1524,'SONDA348','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1525,'SONDA349','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1526,'SONDA350','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1527,'SONDA351','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1528,'SONDA352','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1529,'SONDA353','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1530,'SONDA354','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1531,'SONDA355','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1532,'SONDA356','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1533,'SONDA357','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1534,'SONDA358','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1535,'SONDA359','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1536,'SONDA360','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1537,'SONDA361','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1538,'SONDA362','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1539,'SONDA363','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1540,'SONDA364','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1541,'SONDA365','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1542,'SONDA366','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1543,'SONDA367','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1544,'SONDA368','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1545,'SONDA369','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1546,'SONDA370','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1547,'SONDA371','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1548,'SONDA372','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1549,'SONDA373','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1550,'SONDA374','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1551,'SONDA375','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1552,'SONDA376','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1553,'SONDA377','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1554,'SONDA378','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1555,'SONDA379','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1556,'SONDA380','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1557,'SONDA381','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1558,'SONDA382','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1559,'SONDA383','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1560,'SONDA384','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1561,'SONDA385','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1562,'SONDA386','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1563,'SONDA387','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1564,'SONDA388','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1565,'SONDA389','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1566,'SONDA390','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1567,'SONDA391','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1568,'SONDA392','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1569,'SONDA393','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1570,'SONDA394','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1571,'SONDA395','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1572,'SONDA396','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1573,'SONDA397','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1574,'SONDA398','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1575,'SONDA399','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1576,'SONDA400','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1577,'SONDA401','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1578,'SONDA402','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1579,'SONDA403','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1580,'SONDA404','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1581,'SONDA405','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1582,'SONDA406','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1583,'SONDA407','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1584,'SONDA408','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1585,'SONDA409','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1586,'SONDA410','04-05-1995',21422702);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1587,'SQM277','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1588,'SQM278','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1589,'SQM279','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1590,'SQM280','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1591,'SQM281','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1592,'SQM282','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1593,'SQM283','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1594,'SQM284','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1595,'SQM285','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1596,'SQM286','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1597,'SQM287','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1598,'SQM288','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1599,'SQM289','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1600,'SQM290','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1601,'SQM291','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1602,'SQM292','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1603,'SQM293','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1604,'SQM294','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1605,'SQM295','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1606,'SQM296','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1607,'SQM297','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1608,'SQM298','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1609,'SQM299','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1610,'SQM300','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1611,'SQM301','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1612,'SQM302','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1613,'SQM303','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1614,'SQM304','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1615,'SQM305','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1616,'SQM306','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1617,'SQM307','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1618,'SQM308','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1619,'SQM309','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1620,'SQM310','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1621,'SQM311','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1622,'SQM312','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1623,'SQM313','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1624,'SQM314','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1625,'SQM315','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1626,'SQM316','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1627,'SQM317','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1628,'SQM318','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1629,'SQM319','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1630,'SQM320','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1631,'SQM321','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1632,'SQM322','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1633,'SQM323','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1634,'SQM324','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1635,'SQM325','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1636,'SQM326','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1637,'SQM327','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1638,'SQM328','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1639,'SQM329','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1640,'SQM330','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1641,'SQM331','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1642,'SQM332','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1643,'SQM333','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1644,'SQM334','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1645,'SQM335','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1646,'SQM336','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1647,'SQM337','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1648,'SQM338','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1649,'SQM339','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1650,'SQM340','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1651,'SQM341','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1652,'SQM342','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1653,'SQM343','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1654,'SQM344','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1655,'SQM345','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1656,'SQM346','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1657,'SQM347','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1658,'SQM348','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1659,'SQM349','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1660,'SQM350','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1661,'SQM351','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1662,'SQM352','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1663,'SQM353','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1664,'SQM354','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1665,'SQM355','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1666,'SQM356','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1667,'SQM357','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1668,'SQM358','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1669,'SQM359','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1670,'SQM360','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1671,'SQM361','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1672,'SQM362','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1673,'SQM363','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1674,'SQM364','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1675,'SQM365','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1676,'SQM366','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1677,'SQM367','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1678,'SQM368','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1679,'SQM369','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1680,'SQM370','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1681,'SQM371','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1682,'SQM372','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1683,'SQM373','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1684,'SQM374','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1685,'SQM375','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1686,'SQM376','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1687,'SQM377','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1688,'SQM378','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1689,'SQM379','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1690,'SQM380','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1691,'SQM381','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1692,'SQM382','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1693,'SQM383','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1694,'SQM384','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1695,'SQM385','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1696,'SQM386','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1697,'SQM387','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1698,'SQM388','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1699,'SQM389','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1700,'SQM390','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1701,'SQM391','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1702,'SQM392','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1703,'SQM393','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1704,'SQM394','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1705,'SQM395','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1706,'SQM396','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1707,'SQM397','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1708,'SQM398','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1709,'SQM399','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1710,'SQM400','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1711,'SQM401','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1712,'SQM402','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1713,'SQM403','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1714,'SQM404','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1715,'SQM405','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1716,'SQM406','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1717,'SQM407','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1718,'SQM408','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1719,'SQM409','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1720,'SQM410','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1721,'SQM411','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1722,'SQM412','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1723,'SQM413','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1724,'SQM414','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1725,'SQM415','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1726,'SQM416','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1727,'SQM417','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1728,'SQM418','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1729,'SQM419','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1730,'SQM420','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1731,'SQM421','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1732,'SQM422','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1733,'SQM423','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1734,'SQM424','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1735,'SQM425','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1736,'SQM426','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1737,'SQM427','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1738,'SQM428','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1739,'SQM429','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1740,'SQM430','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1741,'SQM431','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1742,'SQM432','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1743,'SQM433','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1744,'SQM434','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1745,'SQM435','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1746,'SQM436','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1747,'SQM437','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1748,'SQM438','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1749,'SQM439','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1750,'SQM440','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1751,'SQM441','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1752,'SQM442','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1753,'SQM443','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1754,'SQM444','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1755,'SQM445','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1756,'SQM446','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1757,'SQM447','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1758,'SQM448','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1759,'SQM449','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1760,'SQM450','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1761,'SQM451','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1762,'SQM452','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1763,'SQM453','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1764,'SQM454','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1765,'SQM455','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1766,'SQM456','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1767,'SQM457','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1768,'SQM458','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1769,'SQM459','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1770,'SQM460','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1771,'SQM461','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1772,'SQM462','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1773,'SQM463','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1774,'SQM464','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1775,'SQM465','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1776,'SQM466','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1777,'SQM467','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1778,'SQM468','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1779,'SQM469','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1780,'SQM470','20-12-1986',21690870);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1781,'VAPO278','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1782,'VAPO279','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1783,'VAPO280','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1784,'VAPO281','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1785,'VAPO282','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1786,'VAPO283','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1787,'VAPO284','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1788,'VAPO285','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1789,'VAPO286','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1790,'VAPO287','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1791,'VAPO288','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1792,'VAPO289','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1793,'VAPO290','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1794,'VAPO291','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1795,'VAPO292','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1796,'VAPO293','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1797,'VAPO294','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1798,'VAPO295','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1799,'VAPO296','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1800,'VAPO297','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1801,'VAPO298','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1802,'VAPO299','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1803,'VAPO300','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1804,'VAPO301','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1805,'VAPO302','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1806,'VAPO303','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1807,'VAPO304','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1808,'VAPO305','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1809,'VAPO306','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1810,'VAPO307','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1811,'VAPO308','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1812,'VAPO309','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1813,'VAPO310','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1814,'VAPO311','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1815,'VAPO312','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1816,'VAPO313','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1817,'VAPO314','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1818,'VAPO315','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1819,'VAPO316','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1820,'VAPO317','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1821,'VAPO318','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1822,'VAPO319','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1823,'VAPO320','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1824,'VAPO321','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1825,'VAPO322','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1826,'VAPO323','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1827,'VAPO324','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1828,'VAPO325','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1829,'VAPO326','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1830,'VAPO327','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1831,'VAPO328','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1832,'VAPO329','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1833,'VAPO330','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1834,'VAPO331','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1835,'VAPO332','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1836,'VAPO333','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1837,'VAPO334','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1838,'VAPO335','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1839,'VAPO336','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1840,'VAPO337','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1841,'VAPO338','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1842,'VAPO339','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1843,'VAPO340','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1844,'VAPO341','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1845,'VAPO342','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1846,'VAPO343','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1847,'VAPO344','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1848,'VAPO345','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1849,'VAPO346','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1850,'VAPO347','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1851,'VAPO348','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1852,'VAPO349','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1853,'VAPO350','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1854,'VAPO351','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1855,'VAPO352','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1856,'VAPO353','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1857,'VAPO354','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1858,'VAPO355','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1859,'VAPO356','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1860,'VAPO357','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1861,'VAPO358','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1862,'VAPO359','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1863,'VAPO360','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1864,'VAPO361','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1865,'VAPO362','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1866,'VAPO363','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1867,'VAPO364','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1868,'VAPO365','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1869,'VAPO366','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1870,'VAPO367','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1871,'VAPO368','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1872,'VAPO369','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1873,'VAPO370','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1874,'VAPO371','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1875,'VAPO372','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1876,'VAPO373','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1877,'VAPO374','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1878,'VAPO375','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1879,'VAPO376','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1880,'VAPO377','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1881,'VAPO378','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1882,'VAPO379','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1883,'VAPO380','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1884,'VAPO381','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1885,'VAPO382','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1886,'VAPO383','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1887,'VAPO384','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1888,'VAPO385','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1889,'VAPO386','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1890,'VAPO387','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1891,'VAPO388','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1892,'VAPO389','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1893,'VAPO390','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1894,'VAPO391','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1895,'VAPO392','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1896,'VAPO393','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1897,'VAPO394','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1898,'VAPO395','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1899,'VAPO396','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1900,'VAPO397','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1901,'VAPO398','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1902,'VAPO399','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1903,'VAPO400','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1904,'VAPO401','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1905,'VAPO402','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1906,'VAPO403','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1907,'VAPO404','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1908,'VAPO405','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1909,'VAPO406','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1910,'VAPO407','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1911,'VAPO408','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1912,'VAPO409','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1913,'VAPO410','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1914,'VAPO411','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1915,'VAPO412','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1916,'VAPO413','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1917,'VAPO414','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1918,'VAPO415','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1919,'VAPO416','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1920,'VAPO417','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1921,'VAPO418','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1922,'VAPO419','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1923,'VAPO420','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1924,'VAPO421','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1925,'VAPO422','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1926,'VAPO423','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1927,'VAPO424','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1928,'VAPO425','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1929,'VAPO426','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1930,'VAPO427','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1931,'VAPO428','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1932,'VAPO429','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1933,'VAPO430','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1934,'VAPO431','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1935,'VAPO432','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1936,'VAPO433','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1937,'VAPO434','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1938,'VAPO435','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1939,'VAPO436','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1940,'VAPO437','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1941,'VAPO438','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1942,'VAPO439','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1943,'VAPO440','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1944,'VAPO441','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1945,'VAPO442','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1946,'VAPO443','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1947,'VAPO444','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1948,'VAPO445','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1949,'VAPO446','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1950,'VAPO447','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1951,'VAPO448','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1952,'VAPO449','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1953,'VAPO450','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1954,'VAPO451','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1955,'VAPO452','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1956,'VAPO453','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1957,'VAPO454','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1958,'VAPO455','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1959,'VAPO456','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1960,'VAPO457','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1961,'VAPO458','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1962,'VAPO459','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1963,'VAPO460','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1964,'VAPO461','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1965,'VAPO462','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1966,'VAPO463','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1967,'VAPO464','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1968,'VAPO465','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1969,'VAPO466','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1970,'VAPO467','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1971,'VAPO468','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1972,'VAPO469','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1973,'VAPO470','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1974,'VAPO471','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1975,'VAPO472','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1976,'VAPO473','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1977,'VAPO474','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1978,'VAPO475','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1979,'VAPO476','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1980,'VAPO477','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1981,'VAPO478','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1982,'VAPO479','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1983,'VAPO480','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1984,'VAPO481','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1985,'VAPO482','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1986,'VAPO483','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1987,'VAPO484','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1988,'VAPO485','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1989,'VAPO486','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1990,'VAPO487','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1991,'VAPO488','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1992,'VAPO489','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1993,'VAPO490','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1994,'VAPO491','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1995,'VAPO492','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1996,'VAPO493','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1997,'VAPO494','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1998,'VAPO495','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (1999,'VAPO496','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2000,'VAPO497','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2001,'VAPO498','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2002,'VAPO499','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2003,'VAPO500','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2004,'VAPO501','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2005,'VAPO502','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2006,'VAPO503','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2007,'VAPO504','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2008,'VAPO505','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2009,'VAPO506','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2010,'VAPO507','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2011,'VAPO508','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2012,'VAPO509','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2013,'VAPO510','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2014,'VAPO511','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2015,'VAPO512','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2016,'VAPO513','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2017,'VAPO514','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2018,'VAPO515','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2019,'VAPO516','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2020,'VAPO517','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2021,'VAPO518','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2022,'VAPO519','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2023,'VAPO520','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2024,'VAPO521','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2025,'VAPO522','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2026,'VAPO523','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2027,'VAPO524','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2028,'VAPO525','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2029,'VAPO526','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2030,'VAPO527','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2031,'VAPO528','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2032,'VAPO529','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2033,'VAPO530','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2034,'VAPO531','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2035,'VAPO532','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2036,'VAPO533','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2037,'VAPO534','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2038,'VAPO535','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2039,'VAPO536','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2040,'VAPO537','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2041,'VAPO538','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2042,'VAPO539','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2043,'VAPO540','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2044,'VAPO541','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2045,'VAPO542','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2046,'VAPO543','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2047,'VAPO544','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2048,'VAPO545','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2049,'VAPO546','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2050,'VAPO547','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2051,'VAPO548','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2052,'VAPO549','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2053,'VAPO550','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2054,'VAPO551','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2055,'VAPO552','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2056,'VAPO553','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2057,'VAPO554','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2058,'VAPO555','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2059,'VAPO556','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2060,'VAPO557','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2061,'VAPO558','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2062,'VAPO559','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2063,'VAPO560','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2064,'VAPO561','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2065,'VAPO562','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2066,'VAPO563','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2067,'VAPO564','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2068,'VAPO565','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2069,'VAPO566','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2070,'VAPO567','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2071,'VAPO568','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2072,'VAPO569','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2073,'VAPO570','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2074,'VAPO571','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2075,'VAPO572','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2076,'VAPO573','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2077,'VAPO574','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2078,'VAPO575','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2079,'VAPO576','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2080,'VAPO577','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2081,'VAPO578','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2082,'VAPO579','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2083,'VAPO580','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2084,'VAPO581','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2085,'VAPO582','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2086,'VAPO583','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2087,'VAPO584','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2088,'VAPO585','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2089,'VAPO586','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2090,'VAPO587','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2091,'VAPO588','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2092,'VAPO589','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2093,'VAPO590','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2094,'VAPO591','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2095,'VAPO592','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2096,'VAPO593','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2097,'VAPO594','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2098,'VAPO595','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2099,'VAPO596','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2100,'VAPO597','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2101,'VAPO598','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2102,'VAPO599','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2103,'VAPO600','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2104,'VAPO601','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2105,'VAPO602','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2106,'VAPO603','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2107,'VAPO604','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2108,'VAPO605','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2109,'VAPO606','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2110,'VAPO607','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2111,'VAPO608','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2112,'VAPO609','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2113,'VAPO610','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2114,'VAPO611','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2115,'VAPO612','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2116,'VAPO613','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2117,'VAPO614','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2118,'VAPO615','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2119,'VAPO616','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2120,'VAPO617','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2121,'VAPO618','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2122,'VAPO619','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2123,'VAPO620','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2124,'VAPO621','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2125,'VAPO622','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2126,'VAPO623','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2127,'VAPO624','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2128,'VAPO625','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2129,'VAPO626','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2130,'VAPO627','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2131,'VAPO628','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2132,'VAPO629','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2133,'VAPO630','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2134,'VAPO631','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2135,'VAPO632','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2136,'VAPO633','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2137,'VAPO634','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2138,'VAPO635','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2139,'VAPO636','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2140,'VAPO637','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2141,'VAPO638','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2142,'VAPO639','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2143,'VAPO640','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2144,'VAPO641','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2145,'VAPO642','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2146,'VAPO643','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2147,'VAPO644','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2148,'VAPO645','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2149,'VAPO646','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2150,'VAPO647','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2151,'VAPO648','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2152,'VAPO649','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2153,'VAPO650','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2154,'VAPO651','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2155,'VAPO652','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2156,'VAPO653','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2157,'VAPO654','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2158,'VAPO655','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2159,'VAPO656','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2160,'VAPO657','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2161,'VAPO658','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2162,'VAPO659','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2163,'VAPO660','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2164,'VAPO661','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2165,'VAPO662','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2166,'VAPO663','21-12-1993',13858632);
insert into ACCION (codigo_accion ,	descripcion,fecha_emision ,	EMPRESA_rut) values (2167,'VAPO664','21-12-1993',13858632);

insert into TIPO_CUENTA (DESCRIPCION) VALUES('Cuenta Corriente');
insert into TIPO_CUENTA (DESCRIPCION) VALUES('Cuenta Vista');

insert into CUENTA_BANCARIA(nro_cuenta ,banco,	USUARIO_rut ,TIPO_CUENTA_id ) 
values
('5511'		,'SANTANDER'		,	'21547343-0',	1),
('4522'		,'BANCO DE CHILE'	,	'11509611-7',	2),
('9033'		,'BCI'				,	'22481678-2',	1),
('9494'		,'ITAU'				,	'6067897-9',	2),
('585'		,'BANCO DE CHILE'	,	'13321389-9',	1),
('766'		,'SANTANDER'		,	'16247535-5',	2),
('677'		,'BANCO DE CHILE'	,	'17226690-8',	1),
('588'		,'BCI'				,	'10476085-6',	2),
('499'		,'ITAU'				,	'8412013-8',	1),
('4101'		,'BANCO DE CHILE'	,	'10185418-3',	2),
('3112'		,'SANTANDER'		, 	'9397378-k',	1),
('3123'		,'BANCO DE CHILE'	,	'12296627-5',	2),
('2134'		,'BCI'				,	'21417023-k',	1),
('1145'		,'ITAU'				,	'16640642-0',	2),
('0156'		,'BANCO DE CHILE'	,	'7638833-4',	1),
('8167'		,'SANTANDER'		, 	'5131281-3',	2),
('7178'		,'BANCO DE CHILE'	, 	'21296593-6',	1),
('7198'		,'BCI'				, 	'24812644-2',	2),
('6190'		,'ITAU'				,	'19263854-2',	1),
('5201'		,'BANCO DE CHILE'	,	'13103822-4',	2),
('4212'		,'SANTANDER'		,	'11048524-7',	1),
('3223'		,'BANCO DE CHILE'	,	'22810378-0',	2),
('2234'		,'BCI'				,	'21517996-6',	1),
('1245'		,'ITAU'				,	'5602992-3',	2),
('2555'		,'BANCO DE CHILE'	,	'15730821-1',	1),
('2678'		,'SANTANDER'		,	'5538372-3',	2);

insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 1',		'10-01-2010',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'11462052-1');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 2',		'15-01-2011',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'11509611-7');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 3',		'20-12-2016',	'15-12-2025'	,true	,'Siempre gana el corredor'	,0.3	,B'10'::bit(1000),	'22481678-2');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 4',		'15-10-2014',	'20-07-2029'	,true	,'Siempre gana el corredor'	,0.4	,B'10'::bit(1000),	'6067897-9');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 5',	'20-01-2001',	'20-08-2020'	,true	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'13321389-9');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 6',		'30-01-2000',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'16247535-5');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 7',		'29-01-2010',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'17226690-8');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 8',		'10-01-2015',	'15-12-2025'	,true	,'Siempre gana el corredor'	,0.6	,B'10'::bit(1000),	'10476085-6');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 9',		'11-01-2016',	'20-07-2029'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'8412013-8');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 10',	'12-01-2019',	'20-08-2020'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'10185418-3');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 11',	'13-01-2020',	'31-12-2021'	,false	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'9397378-k');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 12',	'14-01-2001',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'12296627-5');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 13',	'15-01-2016',	'15-12-2025'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'21417023-k');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 14',	'16-01-2007',	'20-07-2029'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'16640642-0');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 15',	'17-01-2009',	'20-08-2020'	,true	,'Siempre gana el corredor'	,0.3	,B'10'::bit(1000),	'7638833-4');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 16',	'18-01-2005',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.4	,B'10'::bit(1000),	'5131281-3');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 17',	'19-01-2003',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'21296593-6');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 18',	'20-01-2014',	'15-12-2025'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'24812644-2');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 19',	'11-01-2012',	'20-07-2029'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'19263854-2');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 20',	'15-01-2020',	'20-08-2020'	,false	,'Siempre gana el corredor'	,0.6	,B'10'::bit(1000),	'13103822-4');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 21',	'25-01-2000',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'11048524-7');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 22',	'16-01-2001',	'31-12-2021'	,true	,'Siempre gana el corredor'	,0.2	,B'10'::bit(1000),	'22810378-0');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 23',	'26-01-2002',	'15-12-2025'	,true	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'21517996-6');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 24',	'17-01-2005',	'20-07-2029'	,true	,'Siempre gana el corredor'	,0.1	,B'10'::bit(1000),	'5602992-3');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 25',	'27-01-2008',	'20-08-2020'	,true	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'15730821-1');
insert into CONTRATO (direccion, fecha_contratacion ,fecha_finalizacion ,esta_firmado ,terminos_y_condiciones ,comision , pdf,rut_emisor_contrato ) values ('DIRECCION 26',	'28-01-2015',	'31-12-2020'	,true	,'Siempre gana el corredor'	,0.5	,B'10'::bit(1000),	'5538372-3');


insert into ORDEN (nro_orden,precio_accion,fecha_transaccion,usuario_rut)
values
(1,	1805,	'11-12-2020'	,'11462052-1'),
(2,	3587,	'23-06-2020'	,'11509611-7'),
(3,	1083,	'25-06-2020'	,'22481678-2'),
(4,	2397,	'10-01-2020'	,'6067897-9'),
(5,	1452,	'28-06-2020'	,'13321389-9'),
(6,	3112,	'18-02-2020'	,'16247535-5'),
(7,	1775,	'05-10-2020'	,'17226690-8'),
(8,	1985,	'21-04-2020'	,'10476085-6'),
(9,	796,	'28-09-2020'	,'8412013-8'),
(10,	2614,	'13-06-2020'	,'10185418-3'),
(11,	1709,	'03-07-2020'	,'9397378-k'),
(12,	1056,	'02-11-2020'	,'12296627-5'),
(13,	622,	'16-11-2020'	,'21417023-k'),
(14,	2246,	'03-12-2020'	,'16640642-0'),
(15,	2316,	'08-12-2020'	,'7638833-4'),
(16,	3744,	'10-10-2020'	,'5131281-3'),
(17,	4914,	'04-11-2020'	,'21296593-6'),
(18,	1939,	'16-06-2020'	,'24812644-2'),
(19,	4054,	'24-07-2020'	,'19263854-2'),
(20,	1815,	'09-10-2020'	,'13103822-4'),
(21,	1392,	'04-11-2020'	,'11048524-7'),
(22,	2835,	'27-07-2020'	,'22810378-0'),
(23,	3937,	'09-01-2020'	,'21517996-6'),
(24,	1126,	'11-04-2020'	,'5602992-3'),
(25,	2752,	'09-10-2020'	,'15730821-1'),
(26,	4163,	'12-08-2020'	,'5538372-3');



insert into tipo_orden (descripcion ,ORDEN_nro_orden )
values
('C'	,	1	),
('C'	,	2	),
('C'	,	3	),
('C'	,	4	),
('C'	,	5	),
('C'	,	6	),
('C'	,	7	),
('C'	,	8	),
('C'	,	9	),
('C'	,	10	),
('C'	,	11	),
('C'	,	12	),
('C'	,	13	),
('C'	,	14	),
('C'	,	15	),
('C'	,	16	),
('C'	,	17	),
('C'	,	18	),
('C'	,	19	),
('C'	,	20	),
('C'	,	21	),
('C'	,	22	),
('C'	,	23	),
('C'	,	24	),
('C'	,	25	),
('C'	,	26	);



--ORDENES
insert into ORDEN (nro_orden,precio_accion,fecha_transaccion,usuario_rut)
values
(	27	,	1025	,'05-08-2020','11462052-1'),
(	28	,	4502	,'30-01-2020','11509611-7'),
(	29	,	1961	,'07-03-2020','22481678-2'),
(	30	,	4793	,'24-09-2020','6067897-9'),
(	31	,	1238	,'10-12-2020','13321389-9'),
(	32	,	3742	,'28-08-2020','16247535-5'),
(	33	,	2908	,'13-11-2020','17226690-8'),
(	34	,	1344	,'09-11-2020','10476085-6'),
(	35	,	4493	,'24-01-2020','8412013-8'),
(	36	,	4963	,'13-05-2020','10185418-3'),
(	37	,	1176	,'20-03-2020','9397378-k'),
(	38	,	620		,'10-05-2020','12296627-5'),
(	39	,	1615	,'18-10-2020','21417023-k'),
(	40	,	4418	,'13-11-2020','16640642-0'),
(	41	,	3157	,'18-02-2020','7638833-4'),
(	42	,	4963	,'14-12-2020','5131281-3'),
(	43	,	4042	,'09-10-2020','21296593-6'),
(	44	,	2299	,'02-08-2020','24812644-2'),
(	45	,	1122	,'21-01-2020','19263854-2'),
(	46	,	4468	,'07-07-2020','13103822-4'),
(	47	,	2995	,'23-10-2020','11048524-7'),
(	48	,	991		,'19-04-2020','22810378-0'),
(	49	,	1832	,'05-12-2020','21517996-6'),
(	50	,	4209	,'27-10-2020','5602992-3'),
(	51	,	881		,'13-05-2020','15730821-1'),
(	52	,	4077	,'01-08-2020','5538372-3');

--VENTA
insert into tipo_orden (descripcion ,ORDEN_nro_orden )
values
('V'	,	27	),
('V'	,	28	),
('V'	,	29	),
('V'	,	30	),
('V'	,	31	),
('V'	,	32	),
('V'	,	33	),
('V'	,	34	),
('V'	,	35	),
('V'	,	36	),
('V'	,	37	),
('V'	,	38	),
('V'	,	39	),
('V'	,	40	),
('V'	,	41	),
('V'	,	42	),
('V'	,	43	),
('V'	,	44	),
('V'	,	45	),
('V'	,	46	),
('V'	,	47	),
('V'	,	48	),
('V'	,	49	),
('V'	,	50	),
('V'	,	51	),
('V'	,	52	);



insert into tipo_estado (id,descripcion) values (1,'Pendiente');
insert into tipo_estado (id,descripcion) values (2,'Realizada');
insert into tipo_estado (id,descripcion) values (3,'Cancelada');



insert into DETALLE_ORDEN (ORDEN_nro_orden ,	ACCION_codigo_accion ,	TIPO_ESTADO_id )
values 
(	44	,	1	,	1	),
(	11	,	2	,	1	),
(	16	,	3	,	1	),
(	51	,	4	,	1	),
(	18	,	5	,	1	),
(	10	,	6	,	1	),
(	9	,	7	,	1	),
(	11	,	8	,	1	),
(	11	,	9	,	1	),
(	24	,	10	,	1	),
(	4	,	11	,	1	),
(	12	,	12	,	1	),
(	33	,	13	,	1	),
(	50	,	14	,	1	),
(	33	,	15	,	1	),
(	12	,	16	,	1	),
(	35	,	17	,	1	),
(	20	,	18	,	1	),
(	32	,	19	,	1	),
(	22	,	20	,	1	),
(	9	,	21	,	1	),
(	29	,	22	,	1	),
(	37	,	23	,	1	),
(	49	,	24	,	1	),
(	6	,	25	,	1	),
(	48	,	26	,	1	),
(	30	,	27	,	1	),
(	37	,	28	,	1	),
(	24	,	29	,	1	),
(	33	,	30	,	1	),
(	33	,	31	,	1	),
(	18	,	32	,	1	),
(	45	,	33	,	1	),
(	38	,	34	,	1	),
(	12	,	35	,	1	),
(	39	,	36	,	1	),
(	11	,	37	,	1	),
(	40	,	38	,	1	),
(	46	,	39	,	1	),
(	52	,	40	,	1	),
(	29	,	41	,	1	),
(	5	,	42	,	1	),
(	8	,	43	,	1	),
(	21	,	44	,	1	),
(	40	,	45	,	1	),
(	38	,	46	,	1	),
(	33	,	47	,	1	),
(	23	,	48	,	1	),
(	4	,	49	,	1	),
(	29	,	50	,	1	),
(	26	,	51	,	1	),
(	48	,	52	,	1	),
(	52	,	53	,	1	),
(	23	,	54	,	1	),
(	24	,	55	,	1	),
(	13	,	56	,	1	),
(	33	,	57	,	1	),
(	34	,	58	,	1	),
(	23	,	59	,	1	),
(	42	,	60	,	1	),
(	42	,	61	,	1	),
(	8	,	62	,	1	),
(	27	,	63	,	1	),
(	2	,	64	,	1	),
(	25	,	65	,	1	),
(	47	,	66	,	1	),
(	35	,	67	,	1	),
(	10	,	68	,	1	),
(	29	,	69	,	1	),
(	40	,	70	,	1	),
(	29	,	71	,	1	),
(	19	,	72	,	1	),
(	27	,	73	,	1	),
(	27	,	74	,	1	),
(	51	,	75	,	1	),
(	41	,	76	,	1	),
(	35	,	77	,	1	),
(	15	,	78	,	1	),
(	26	,	79	,	1	),
(	5	,	80	,	1	),
(	46	,	81	,	1	),
(	9	,	82	,	1	),
(	19	,	83	,	1	),
(	8	,	84	,	1	),
(	47	,	85	,	1	),
(	50	,	86	,	1	),
(	33	,	87	,	1	),
(	2	,	88	,	1	),
(	39	,	89	,	1	),
(	41	,	90	,	1	),
(	15	,	91	,	1	),
(	39	,	92	,	1	),
(	21	,	93	,	1	),
(	28	,	94	,	1	),
(	14	,	95	,	1	),
(	47	,	96	,	1	),
(	46	,	97	,	1	),
(	14	,	98	,	1	),
(	8	,	99	,	1	),
(	47	,	100	,	1	),
(	44	,	101	,	1	),
(	50	,	102	,	1	),
(	44	,	103	,	1	),
(	28	,	104	,	1	),
(	9	,	105	,	1	),
(	9	,	106	,	1	),
(	31	,	107	,	1	),
(	10	,	108	,	1	),
(	18	,	109	,	1	),
(	44	,	110	,	1	),
(	23	,	111	,	1	),
(	26	,	112	,	1	),
(	21	,	113	,	1	),
(	15	,	114	,	1	),
(	1	,	115	,	1	),
(	52	,	116	,	1	),
(	5	,	117	,	1	),
(	5	,	118	,	1	),
(	24	,	119	,	1	),
(	34	,	120	,	1	),
(	17	,	121	,	1	),
(	45	,	122	,	1	),
(	4	,	123	,	1	),
(	12	,	124	,	1	),
(	12	,	125	,	1	),
(	23	,	126	,	1	),
(	32	,	127	,	1	),
(	33	,	128	,	1	),
(	26	,	129	,	1	),
(	10	,	130	,	1	),
(	28	,	131	,	1	),
(	16	,	132	,	1	),
(	34	,	133	,	1	),
(	31	,	134	,	1	),
(	36	,	135	,	1	),
(	7	,	136	,	1	),
(	7	,	137	,	1	),
(	17	,	138	,	1	),
(	51	,	139	,	1	),
(	21	,	140	,	1	),
(	48	,	141	,	1	),
(	29	,	142	,	1	),
(	49	,	143	,	1	),
(	50	,	144	,	1	),
(	24	,	145	,	1	),
(	18	,	146	,	1	),
(	16	,	147	,	1	),
(	15	,	148	,	1	),
(	36	,	149	,	1	),
(	21	,	150	,	1	),
(	5	,	151	,	1	),
(	47	,	152	,	1	),
(	17	,	153	,	1	),
(	26	,	154	,	1	),
(	28	,	155	,	1	),
(	43	,	156	,	1	),
(	5	,	157	,	1	),
(	34	,	158	,	1	),
(	10	,	159	,	1	),
(	36	,	160	,	1	),
(	36	,	161	,	1	),
(	26	,	162	,	1	),
(	27	,	163	,	1	),
(	41	,	164	,	1	),
(	27	,	165	,	1	),
(	2	,	166	,	1	),
(	4	,	167	,	1	),
(	18	,	168	,	1	),
(	12	,	169	,	1	),
(	11	,	170	,	1	),
(	47	,	171	,	1	),
(	2	,	172	,	1	),
(	22	,	173	,	1	),
(	21	,	174	,	1	),
(	19	,	175	,	1	),
(	34	,	176	,	1	),
(	18	,	177	,	1	),
(	18	,	178	,	1	),
(	22	,	179	,	1	),
(	40	,	180	,	1	),
(	16	,	181	,	1	),
(	13	,	182	,	1	),
(	12	,	183	,	1	),
(	29	,	184	,	1	),
(	10	,	185	,	1	),
(	28	,	186	,	1	),
(	49	,	187	,	1	),
(	49	,	188	,	1	),
(	18	,	189	,	1	),
(	32	,	190	,	1	),
(	8	,	191	,	1	),
(	33	,	192	,	1	),
(	19	,	193	,	1	),
(	44	,	194	,	1	),
(	15	,	195	,	1	),
(	33	,	196	,	1	),
(	25	,	197	,	1	),
(	8	,	198	,	1	),
(	30	,	199	,	1	),
(	32	,	200	,	1	),
(	33	,	201	,	1	),
(	30	,	202	,	1	),
(	21	,	203	,	1	),
(	26	,	204	,	1	),
(	10	,	205	,	1	),
(	38	,	206	,	1	),
(	14	,	207	,	1	),
(	24	,	208	,	1	),
(	40	,	209	,	1	),
(	38	,	210	,	1	),
(	49	,	211	,	1	),
(	20	,	212	,	1	),
(	24	,	213	,	1	),
(	18	,	214	,	1	),
(	44	,	215	,	1	),
(	31	,	216	,	1	),
(	15	,	217	,	1	),
(	14	,	218	,	1	),
(	17	,	219	,	1	),
(	29	,	220	,	1	),
(	35	,	221	,	1	),
(	39	,	222	,	1	),
(	31	,	223	,	1	),
(	40	,	224	,	1	),
(	38	,	225	,	1	),
(	12	,	226	,	1	),
(	17	,	227	,	1	),
(	34	,	228	,	1	),
(	30	,	229	,	1	),
(	48	,	230	,	1	),
(	29	,	231	,	1	),
(	5	,	232	,	1	),
(	35	,	233	,	1	),
(	1	,	234	,	1	),
(	50	,	235	,	1	),
(	20	,	236	,	1	),
(	20	,	237	,	1	),
(	37	,	238	,	1	),
(	28	,	239	,	1	),
(	40	,	240	,	1	),
(	20	,	241	,	1	),
(	25	,	242	,	1	),
(	24	,	243	,	1	),
(	49	,	244	,	1	),
(	18	,	245	,	1	),
(	19	,	246	,	1	),
(	43	,	247	,	1	),
(	10	,	248	,	1	),
(	39	,	249	,	1	),
(	37	,	250	,	1	),
(	37	,	251	,	1	),
(	21	,	252	,	1	),
(	9	,	253	,	1	),
(	4	,	254	,	1	),
(	6	,	255	,	1	),
(	10	,	256	,	1	),
(	9	,	257	,	1	),
(	33	,	258	,	1	),
(	4	,	259	,	1	),
(	1	,	260	,	1	),
(	34	,	261	,	1	),
(	45	,	262	,	1	),
(	27	,	263	,	1	),
(	24	,	264	,	1	),
(	28	,	265	,	1	),
(	38	,	266	,	1	),
(	41	,	267	,	1	),
(	44	,	268	,	1	),
(	52	,	269	,	1	),
(	34	,	270	,	1	),
(	2	,	271	,	1	),
(	21	,	272	,	1	),
(	23	,	273	,	1	),
(	37	,	274	,	1	),
(	45	,	275	,	1	),
(	12	,	276	,	1	),
(	26	,	277	,	1	),
(	4	,	278	,	1	),
(	7	,	279	,	1	),
(	38	,	280	,	1	),
(	20	,	281	,	1	),
(	12	,	282	,	1	),
(	25	,	283	,	1	),
(	12	,	284	,	1	),
(	48	,	285	,	1	),
(	9	,	286	,	1	),
(	11	,	287	,	1	),
(	11	,	288	,	1	),
(	44	,	289	,	1	),
(	20	,	290	,	1	),
(	6	,	291	,	1	),
(	34	,	292	,	1	),
(	19	,	293	,	1	),
(	36	,	294	,	1	),
(	14	,	295	,	1	),
(	8	,	296	,	1	),
(	33	,	297	,	1	),
(	9	,	298	,	1	),
(	9	,	299	,	1	),
(	5	,	300	,	1	),
(	37	,	301	,	1	),
(	41	,	302	,	1	),
(	45	,	303	,	1	),
(	13	,	304	,	1	),
(	24	,	305	,	1	),
(	48	,	306	,	1	),
(	46	,	307	,	1	),
(	2	,	308	,	1	),
(	52	,	309	,	1	),
(	37	,	310	,	1	),
(	7	,	311	,	1	),
(	28	,	312	,	1	),
(	7	,	313	,	1	),
(	40	,	314	,	1	),
(	23	,	315	,	1	),
(	9	,	316	,	1	),
(	32	,	317	,	1	),
(	42	,	318	,	1	),
(	6	,	319	,	1	),
(	2	,	320	,	1	),
(	41	,	321	,	1	),
(	36	,	322	,	1	),
(	2	,	323	,	1	),
(	42	,	324	,	1	),
(	33	,	325	,	1	),
(	31	,	326	,	1	),
(	4	,	327	,	1	),
(	41	,	328	,	1	),
(	29	,	329	,	1	),
(	50	,	330	,	1	),
(	4	,	331	,	1	),
(	13	,	332	,	1	),
(	39	,	333	,	1	),
(	22	,	334	,	1	),
(	29	,	335	,	1	),
(	12	,	336	,	1	),
(	3	,	337	,	1	),
(	26	,	338	,	1	),
(	41	,	339	,	1	),
(	43	,	340	,	1	),
(	33	,	341	,	1	),
(	14	,	342	,	1	),
(	16	,	343	,	1	),
(	19	,	344	,	1	),
(	36	,	345	,	1	),
(	49	,	346	,	1	),
(	33	,	347	,	1	),
(	24	,	348	,	1	),
(	27	,	349	,	1	),
(	11	,	350	,	1	),
(	9	,	351	,	1	),
(	3	,	352	,	1	),
(	24	,	353	,	1	),
(	26	,	354	,	1	),
(	17	,	355	,	1	),
(	2	,	356	,	1	),
(	30	,	357	,	1	),
(	27	,	358	,	1	),
(	52	,	359	,	1	),
(	29	,	360	,	1	),
(	27	,	361	,	1	),
(	19	,	362	,	1	),
(	46	,	363	,	1	),
(	45	,	364	,	1	),
(	22	,	365	,	1	),
(	47	,	366	,	1	),
(	13	,	367	,	1	),
(	28	,	368	,	1	),
(	26	,	369	,	1	),
(	28	,	370	,	1	),
(	1	,	371	,	1	),
(	18	,	372	,	1	),
(	44	,	373	,	1	),
(	45	,	374	,	1	),
(	19	,	375	,	1	),
(	23	,	376	,	1	),
(	47	,	377	,	1	),
(	11	,	378	,	1	),
(	17	,	379	,	1	),
(	24	,	380	,	1	),
(	8	,	381	,	1	),
(	40	,	382	,	1	),
(	52	,	383	,	1	),
(	43	,	384	,	1	),
(	15	,	385	,	1	),
(	22	,	386	,	1	),
(	21	,	387	,	1	),
(	31	,	388	,	1	),
(	42	,	389	,	1	),
(	30	,	390	,	1	),
(	17	,	391	,	1	),
(	1	,	392	,	1	),
(	40	,	393	,	1	),
(	5	,	394	,	1	),
(	3	,	395	,	1	),
(	26	,	396	,	1	),
(	48	,	397	,	1	),
(	44	,	398	,	1	),
(	39	,	399	,	1	),
(	27	,	400	,	1	),
(	35	,	401	,	1	),
(	39	,	402	,	1	),
(	47	,	403	,	1	),
(	9	,	404	,	1	),
(	7	,	405	,	1	),
(	22	,	406	,	1	),
(	48	,	407	,	1	),
(	13	,	408	,	1	),
(	38	,	409	,	1	),
(	24	,	410	,	1	),
(	18	,	411	,	1	),
(	26	,	412	,	1	),
(	20	,	413	,	1	),
(	8	,	414	,	1	),
(	35	,	415	,	1	),
(	16	,	416	,	1	),
(	12	,	417	,	1	),
(	3	,	418	,	1	),
(	44	,	419	,	1	),
(	35	,	420	,	1	),
(	10	,	421	,	1	),
(	2	,	422	,	1	),
(	8	,	423	,	1	),
(	22	,	424	,	1	),
(	32	,	425	,	1	),
(	48	,	426	,	1	),
(	48	,	427	,	1	),
(	27	,	428	,	1	),
(	28	,	429	,	1	),
(	45	,	430	,	1	),
(	3	,	431	,	1	),
(	35	,	432	,	1	),
(	2	,	433	,	1	),
(	22	,	434	,	1	),
(	28	,	435	,	1	),
(	30	,	436	,	1	),
(	20	,	437	,	1	),
(	3	,	438	,	1	),
(	11	,	439	,	1	),
(	37	,	440	,	1	),
(	20	,	441	,	1	),
(	31	,	442	,	1	),
(	11	,	443	,	1	),
(	34	,	444	,	1	),
(	49	,	445	,	1	),
(	52	,	446	,	1	),
(	39	,	447	,	1	),
(	46	,	448	,	1	),
(	48	,	449	,	1	),
(	6	,	450	,	1	),
(	24	,	451	,	1	),
(	25	,	452	,	1	),
(	28	,	453	,	1	),
(	43	,	454	,	1	),
(	12	,	455	,	1	),
(	51	,	456	,	1	),
(	9	,	457	,	1	),
(	45	,	458	,	1	),
(	28	,	459	,	1	),
(	38	,	460	,	1	),
(	32	,	461	,	1	),
(	27	,	462	,	1	),
(	6	,	463	,	1	),
(	7	,	464	,	1	),
(	29	,	465	,	1	),
(	37	,	466	,	1	),
(	21	,	467	,	1	),
(	5	,	468	,	1	),
(	49	,	469	,	1	),
(	34	,	470	,	1	),
(	39	,	471	,	1	),
(	23	,	472	,	1	),
(	5	,	473	,	1	),
(	1	,	474	,	1	),
(	33	,	475	,	1	),
(	4	,	476	,	1	),
(	27	,	477	,	1	),
(	50	,	478	,	1	),
(	26	,	479	,	1	),
(	23	,	480	,	1	),
(	52	,	481	,	1	),
(	28	,	482	,	1	),
(	39	,	483	,	1	),
(	34	,	484	,	1	),
(	45	,	485	,	1	),
(	32	,	486	,	1	),
(	18	,	487	,	1	),
(	8	,	488	,	1	),
(	40	,	489	,	1	),
(	52	,	490	,	1	),
(	22	,	491	,	1	),
(	31	,	492	,	1	),
(	31	,	493	,	1	),
(	50	,	494	,	1	),
(	5	,	495	,	1	),
(	12	,	496	,	1	),
(	30	,	497	,	1	),
(	46	,	498	,	1	),
(	40	,	499	,	1	),
(	2	,	500	,	1	),
(	16	,	501	,	1	),
(	30	,	502	,	1	),
(	29	,	503	,	1	),
(	34	,	504	,	1	),
(	5	,	505	,	1	),
(	31	,	506	,	1	),
(	32	,	507	,	1	),
(	39	,	508	,	1	),
(	15	,	509	,	1	),
(	5	,	510	,	1	),
(	47	,	511	,	1	),
(	20	,	512	,	1	),
(	6	,	513	,	1	),
(	45	,	514	,	1	),
(	45	,	515	,	1	),
(	50	,	516	,	1	),
(	28	,	517	,	1	),
(	50	,	518	,	1	),
(	10	,	519	,	1	),
(	21	,	520	,	1	),
(	17	,	521	,	1	),
(	52	,	522	,	1	),
(	51	,	523	,	1	),
(	6	,	524	,	1	),
(	29	,	525	,	1	),
(	11	,	526	,	1	),
(	43	,	527	,	1	),
(	2	,	528	,	1	),
(	47	,	529	,	1	),
(	3	,	530	,	1	),
(	49	,	531	,	1	),
(	43	,	532	,	1	),
(	28	,	533	,	1	),
(	42	,	534	,	1	),
(	8	,	535	,	1	),
(	39	,	536	,	1	),
(	49	,	537	,	1	),
(	39	,	538	,	1	),
(	43	,	539	,	1	),
(	33	,	540	,	1	),
(	17	,	541	,	1	),
(	2	,	542	,	1	),
(	47	,	543	,	1	),
(	3	,	544	,	1	),
(	41	,	545	,	1	),
(	27	,	546	,	1	),
(	3	,	547	,	1	),
(	16	,	548	,	1	),
(	8	,	549	,	1	),
(	52	,	550	,	1	),
(	25	,	551	,	1	),
(	46	,	552	,	1	),
(	41	,	553	,	1	),
(	10	,	554	,	1	),
(	23	,	555	,	1	),
(	8	,	556	,	1	),
(	45	,	557	,	1	),
(	43	,	558	,	1	),
(	34	,	559	,	1	),
(	30	,	560	,	1	),
(	24	,	561	,	1	),
(	27	,	562	,	1	),
(	28	,	563	,	1	),
(	20	,	564	,	1	),
(	47	,	565	,	1	),
(	25	,	566	,	1	),
(	52	,	567	,	1	),
(	39	,	568	,	1	),
(	18	,	569	,	1	),
(	42	,	570	,	1	),
(	11	,	571	,	1	),
(	36	,	572	,	1	),
(	6	,	573	,	1	),
(	24	,	574	,	1	),
(	33	,	575	,	1	),
(	26	,	576	,	1	),
(	15	,	577	,	1	),
(	25	,	578	,	1	),
(	25	,	579	,	1	),
(	10	,	580	,	1	),
(	26	,	581	,	1	),
(	1	,	582	,	1	),
(	15	,	583	,	1	),
(	47	,	584	,	1	),
(	42	,	585	,	1	),
(	2	,	586	,	1	),
(	41	,	587	,	1	),
(	49	,	588	,	1	),
(	48	,	589	,	1	),
(	23	,	590	,	1	),
(	46	,	591	,	1	),
(	22	,	592	,	1	),
(	14	,	593	,	1	),
(	47	,	594	,	1	),
(	17	,	595	,	1	),
(	36	,	596	,	1	),
(	28	,	597	,	1	),
(	4	,	598	,	1	),
(	50	,	599	,	1	),
(	50	,	600	,	1	),
(	40	,	601	,	1	),
(	46	,	602	,	1	),
(	52	,	603	,	1	),
(	11	,	604	,	1	),
(	40	,	605	,	1	),
(	12	,	606	,	1	),
(	52	,	607	,	1	),
(	1	,	608	,	1	),
(	8	,	609	,	1	),
(	37	,	610	,	1	),
(	4	,	611	,	1	),
(	31	,	612	,	1	),
(	14	,	613	,	1	),
(	41	,	614	,	1	),
(	3	,	615	,	1	),
(	22	,	616	,	1	),
(	24	,	617	,	1	),
(	7	,	618	,	1	),
(	11	,	619	,	1	),
(	14	,	620	,	1	),
(	28	,	621	,	1	),
(	35	,	622	,	1	),
(	19	,	623	,	1	),
(	7	,	624	,	1	),
(	22	,	625	,	1	),
(	17	,	626	,	1	),
(	42	,	627	,	1	),
(	52	,	628	,	1	),
(	24	,	629	,	1	),
(	3	,	630	,	1	),
(	15	,	631	,	1	),
(	31	,	632	,	1	),
(	4	,	633	,	1	),
(	29	,	634	,	1	),
(	23	,	635	,	1	),
(	45	,	636	,	1	),
(	46	,	637	,	1	),
(	5	,	638	,	1	),
(	20	,	639	,	1	),
(	37	,	640	,	1	),
(	15	,	641	,	1	),
(	37	,	642	,	1	),
(	14	,	643	,	1	),
(	1	,	644	,	1	),
(	14	,	645	,	1	),
(	14	,	646	,	1	),
(	32	,	647	,	1	),
(	45	,	648	,	1	),
(	12	,	649	,	1	),
(	36	,	650	,	1	),
(	33	,	651	,	1	),
(	6	,	652	,	1	),
(	12	,	653	,	1	),
(	33	,	654	,	1	),
(	1	,	655	,	1	),
(	18	,	656	,	1	),
(	25	,	657	,	1	),
(	49	,	658	,	1	),
(	44	,	659	,	1	),
(	11	,	660	,	1	),
(	32	,	661	,	1	),
(	36	,	662	,	1	),
(	45	,	663	,	1	),
(	45	,	664	,	1	),
(	8	,	665	,	1	),
(	14	,	666	,	1	),
(	25	,	667	,	1	),
(	19	,	668	,	1	),
(	29	,	669	,	1	),
(	21	,	670	,	1	),
(	24	,	671	,	1	),
(	29	,	672	,	1	),
(	16	,	673	,	1	),
(	40	,	674	,	1	),
(	26	,	675	,	1	),
(	17	,	676	,	1	),
(	26	,	677	,	1	),
(	10	,	678	,	1	),
(	44	,	679	,	1	),
(	47	,	680	,	1	),
(	2	,	681	,	1	),
(	33	,	682	,	1	),
(	7	,	683	,	1	),
(	35	,	684	,	1	),
(	44	,	685	,	1	),
(	25	,	686	,	1	),
(	45	,	687	,	1	),
(	7	,	688	,	1	),
(	1	,	689	,	1	),
(	26	,	690	,	1	),
(	40	,	691	,	1	),
(	50	,	692	,	1	),
(	52	,	693	,	1	),
(	5	,	694	,	1	),
(	46	,	695	,	1	),
(	29	,	696	,	1	),
(	17	,	697	,	1	),
(	27	,	698	,	1	),
(	39	,	699	,	1	),
(	51	,	700	,	1	),
(	28	,	701	,	1	),
(	22	,	702	,	1	),
(	7	,	703	,	1	),
(	20	,	704	,	1	),
(	31	,	705	,	1	),
(	17	,	706	,	1	),
(	3	,	707	,	1	),
(	46	,	708	,	1	),
(	11	,	709	,	1	),
(	5	,	710	,	1	),
(	22	,	711	,	1	),
(	9	,	712	,	1	),
(	41	,	713	,	1	),
(	5	,	714	,	1	),
(	43	,	715	,	1	),
(	1	,	716	,	1	),
(	12	,	717	,	1	),
(	36	,	718	,	1	),
(	49	,	719	,	1	),
(	20	,	720	,	1	),
(	4	,	721	,	1	),
(	14	,	722	,	1	),
(	39	,	723	,	1	),
(	32	,	724	,	1	),
(	19	,	725	,	1	),
(	17	,	726	,	1	),
(	42	,	727	,	1	),
(	18	,	728	,	1	),
(	35	,	729	,	1	),
(	4	,	730	,	1	),
(	25	,	731	,	1	),
(	23	,	732	,	1	),
(	4	,	733	,	1	),
(	3	,	734	,	1	),
(	29	,	735	,	1	),
(	20	,	736	,	1	),
(	32	,	737	,	1	),
(	50	,	738	,	1	),
(	19	,	739	,	1	),
(	28	,	740	,	1	),
(	51	,	741	,	1	),
(	5	,	742	,	1	),
(	34	,	743	,	1	),
(	16	,	744	,	1	),
(	40	,	745	,	1	),
(	9	,	746	,	1	),
(	6	,	747	,	1	),
(	50	,	748	,	1	),
(	20	,	749	,	1	),
(	11	,	750	,	1	),
(	22	,	751	,	1	),
(	21	,	752	,	1	),
(	34	,	753	,	1	),
(	29	,	754	,	1	),
(	37	,	755	,	1	),
(	13	,	756	,	1	),
(	2	,	757	,	1	),
(	11	,	758	,	1	),
(	47	,	759	,	1	),
(	3	,	760	,	1	),
(	34	,	761	,	1	),
(	7	,	762	,	1	),
(	14	,	763	,	1	),
(	10	,	764	,	1	),
(	41	,	765	,	1	),
(	44	,	766	,	1	),
(	24	,	767	,	1	),
(	11	,	768	,	1	),
(	49	,	769	,	1	),
(	46	,	770	,	1	),
(	9	,	771	,	1	),
(	21	,	772	,	1	),
(	3	,	773	,	1	),
(	47	,	774	,	1	),
(	50	,	775	,	1	),
(	14	,	776	,	1	),
(	29	,	777	,	1	),
(	25	,	778	,	1	),
(	27	,	779	,	1	),
(	46	,	780	,	1	),
(	47	,	781	,	1	),
(	40	,	782	,	1	),
(	23	,	783	,	1	),
(	36	,	784	,	1	),
(	15	,	785	,	1	),
(	38	,	786	,	1	),
(	33	,	787	,	1	),
(	21	,	788	,	1	),
(	48	,	789	,	1	),
(	34	,	790	,	1	),
(	20	,	791	,	1	),
(	2	,	792	,	1	),
(	39	,	793	,	1	),
(	38	,	794	,	1	),
(	42	,	795	,	1	),
(	32	,	796	,	1	),
(	27	,	797	,	1	),
(	44	,	798	,	1	),
(	51	,	799	,	1	),
(	16	,	800	,	1	),
(	36	,	801	,	1	),
(	18	,	802	,	1	),
(	39	,	803	,	1	),
(	28	,	804	,	1	),
(	24	,	805	,	1	),
(	27	,	806	,	1	),
(	40	,	807	,	1	),
(	44	,	808	,	1	),
(	6	,	809	,	1	),
(	4	,	810	,	1	),
(	14	,	811	,	1	),
(	14	,	812	,	1	),
(	8	,	813	,	1	),
(	6	,	814	,	1	),
(	32	,	815	,	1	),
(	40	,	816	,	1	),
(	51	,	817	,	1	),
(	14	,	818	,	1	),
(	13	,	819	,	1	),
(	24	,	820	,	1	),
(	7	,	821	,	1	),
(	1	,	822	,	1	),
(	15	,	823	,	1	),
(	1	,	824	,	1	),
(	30	,	825	,	1	),
(	6	,	826	,	1	),
(	29	,	827	,	1	),
(	20	,	828	,	1	),
(	39	,	829	,	1	),
(	39	,	830	,	1	),
(	4	,	831	,	1	),
(	30	,	832	,	1	),
(	19	,	833	,	1	),
(	11	,	834	,	1	),
(	33	,	835	,	1	),
(	8	,	836	,	1	),
(	51	,	837	,	1	),
(	48	,	838	,	1	),
(	31	,	839	,	1	),
(	12	,	840	,	1	),
(	46	,	841	,	1	),
(	17	,	842	,	1	),
(	28	,	843	,	1	),
(	32	,	844	,	1	),
(	41	,	845	,	1	),
(	5	,	846	,	1	),
(	42	,	847	,	1	),
(	8	,	848	,	1	),
(	43	,	849	,	1	),
(	42	,	850	,	1	),
(	26	,	851	,	1	),
(	19	,	852	,	1	),
(	48	,	853	,	1	),
(	13	,	854	,	1	),
(	25	,	855	,	1	),
(	41	,	856	,	1	),
(	37	,	857	,	1	),
(	31	,	858	,	1	),
(	33	,	859	,	1	),
(	26	,	860	,	1	),
(	14	,	861	,	1	),
(	30	,	862	,	1	),
(	8	,	863	,	1	),
(	21	,	864	,	1	),
(	3	,	865	,	1	),
(	34	,	866	,	1	),
(	23	,	867	,	1	),
(	7	,	868	,	1	),
(	13	,	869	,	1	),
(	38	,	870	,	1	),
(	2	,	871	,	1	),
(	3	,	872	,	1	),
(	15	,	873	,	1	),
(	43	,	874	,	1	),
(	19	,	875	,	1	),
(	47	,	876	,	1	),
(	32	,	877	,	1	),
(	5	,	878	,	1	),
(	51	,	879	,	1	),
(	23	,	880	,	1	),
(	40	,	881	,	1	),
(	29	,	882	,	1	),
(	3	,	883	,	1	),
(	20	,	884	,	1	),
(	20	,	885	,	1	),
(	17	,	886	,	1	),
(	13	,	887	,	1	),
(	38	,	888	,	1	),
(	27	,	889	,	1	),
(	31	,	890	,	1	),
(	23	,	891	,	1	),
(	46	,	892	,	1	),
(	36	,	893	,	1	),
(	48	,	894	,	1	),
(	21	,	895	,	1	),
(	15	,	896	,	1	),
(	49	,	897	,	1	),
(	11	,	898	,	1	),
(	16	,	899	,	1	),
(	47	,	900	,	1	),
(	5	,	901	,	1	),
(	36	,	902	,	1	),
(	11	,	903	,	1	),
(	7	,	904	,	1	),
(	39	,	905	,	1	),
(	49	,	906	,	1	),
(	7	,	907	,	1	),
(	19	,	908	,	1	),
(	45	,	909	,	1	),
(	52	,	910	,	1	),
(	43	,	911	,	1	),
(	25	,	912	,	1	),
(	28	,	913	,	1	),
(	35	,	914	,	1	),
(	10	,	915	,	1	),
(	42	,	916	,	1	),
(	44	,	917	,	1	),
(	52	,	918	,	1	),
(	12	,	919	,	1	),
(	46	,	920	,	1	),
(	47	,	921	,	1	),
(	32	,	922	,	1	),
(	39	,	923	,	1	),
(	11	,	924	,	1	),
(	16	,	925	,	1	),
(	45	,	926	,	1	),
(	5	,	927	,	1	),
(	21	,	928	,	1	),
(	16	,	929	,	1	),
(	40	,	930	,	1	),
(	18	,	931	,	1	),
(	23	,	932	,	1	),
(	40	,	933	,	1	),
(	37	,	934	,	1	),
(	14	,	935	,	1	),
(	38	,	936	,	1	),
(	38	,	937	,	1	),
(	42	,	938	,	1	),
(	47	,	939	,	1	),
(	43	,	940	,	1	),
(	31	,	941	,	1	),
(	51	,	942	,	1	),
(	43	,	943	,	1	),
(	14	,	944	,	1	),
(	50	,	945	,	1	),
(	37	,	946	,	1	),
(	47	,	947	,	1	),
(	31	,	948	,	1	),
(	41	,	949	,	1	),
(	47	,	950	,	1	),
(	43	,	951	,	1	),
(	7	,	952	,	1	),
(	36	,	953	,	1	),
(	2	,	954	,	1	),
(	8	,	955	,	1	),
(	6	,	956	,	1	),
(	47	,	957	,	1	),
(	25	,	958	,	1	),
(	16	,	959	,	1	),
(	7	,	960	,	1	),
(	43	,	961	,	1	),
(	25	,	962	,	1	),
(	33	,	963	,	1	),
(	6	,	964	,	1	),
(	27	,	965	,	1	),
(	39	,	966	,	1	),
(	8	,	967	,	1	),
(	41	,	968	,	1	),
(	10	,	969	,	1	),
(	4	,	970	,	1	),
(	10	,	971	,	1	),
(	17	,	972	,	1	),
(	5	,	973	,	1	),
(	16	,	974	,	1	),
(	52	,	975	,	1	),
(	51	,	976	,	1	),
(	31	,	977	,	1	),
(	7	,	978	,	1	),
(	4	,	979	,	1	),
(	8	,	980	,	1	),
(	10	,	981	,	1	),
(	11	,	982	,	1	),
(	48	,	983	,	1	),
(	52	,	984	,	1	),
(	31	,	985	,	1	),
(	34	,	986	,	1	),
(	7	,	987	,	1	),
(	32	,	988	,	1	),
(	7	,	989	,	1	),
(	11	,	990	,	1	),
(	16	,	991	,	1	),
(	21	,	992	,	1	),
(	21	,	993	,	1	),
(	41	,	994	,	1	),
(	41	,	995	,	1	),
(	15	,	996	,	1	),
(	44	,	997	,	1	),
(	51	,	998	,	1	),
(	10	,	999	,	1	),
(	17	,	1000	,	1	),
(	9	,	1001	,	1	),
(	1	,	1002	,	1	),
(	41	,	1003	,	1	),
(	14	,	1004	,	1	),
(	43	,	1005	,	1	),
(	23	,	1006	,	1	),
(	29	,	1007	,	1	),
(	7	,	1008	,	1	),
(	34	,	1009	,	1	),
(	40	,	1010	,	1	),
(	24	,	1011	,	1	),
(	40	,	1012	,	1	),
(	51	,	1013	,	1	),
(	19	,	1014	,	1	),
(	5	,	1015	,	1	),
(	37	,	1016	,	1	),
(	41	,	1017	,	1	),
(	21	,	1018	,	1	),
(	32	,	1019	,	1	),
(	48	,	1020	,	1	),
(	23	,	1021	,	1	),
(	46	,	1022	,	1	),
(	42	,	1023	,	1	),
(	4	,	1024	,	1	),
(	31	,	1025	,	1	),
(	32	,	1026	,	1	),
(	28	,	1027	,	1	),
(	42	,	1028	,	1	),
(	40	,	1029	,	1	),
(	48	,	1030	,	1	),
(	52	,	1031	,	1	),
(	33	,	1032	,	1	),
(	52	,	1033	,	1	),
(	36	,	1034	,	1	),
(	30	,	1035	,	1	),
(	52	,	1036	,	1	),
(	48	,	1037	,	1	),
(	49	,	1038	,	1	),
(	12	,	1039	,	1	),
(	20	,	1040	,	1	),
(	50	,	1041	,	1	),
(	2	,	1042	,	1	),
(	42	,	1043	,	1	),
(	38	,	1044	,	1	),
(	52	,	1045	,	1	),
(	13	,	1046	,	1	),
(	3	,	1047	,	1	),
(	27	,	1048	,	1	),
(	25	,	1049	,	1	),
(	40	,	1050	,	1	),
(	30	,	1051	,	1	),
(	29	,	1052	,	1	),
(	20	,	1053	,	1	),
(	42	,	1054	,	1	),
(	48	,	1055	,	1	),
(	43	,	1056	,	1	),
(	14	,	1057	,	1	),
(	32	,	1058	,	1	),
(	10	,	1059	,	1	),
(	28	,	1060	,	1	),
(	38	,	1061	,	1	),
(	21	,	1062	,	1	),
(	46	,	1063	,	1	),
(	52	,	1064	,	1	),
(	46	,	1065	,	1	),
(	22	,	1066	,	1	),
(	10	,	1067	,	1	),
(	5	,	1068	,	1	),
(	21	,	1069	,	1	),
(	26	,	1070	,	1	),
(	42	,	1071	,	1	),
(	42	,	1072	,	1	),
(	52	,	1073	,	1	),
(	28	,	1074	,	1	),
(	12	,	1075	,	1	),
(	39	,	1076	,	1	),
(	6	,	1077	,	1	),
(	35	,	1078	,	1	),
(	16	,	1079	,	1	),
(	44	,	1080	,	1	),
(	28	,	1081	,	1	),
(	35	,	1082	,	1	),
(	41	,	1083	,	1	),
(	40	,	1084	,	1	),
(	28	,	1085	,	1	),
(	23	,	1086	,	1	),
(	14	,	1087	,	1	),
(	31	,	1088	,	1	),
(	47	,	1089	,	1	),
(	1	,	1090	,	1	),
(	19	,	1091	,	1	),
(	46	,	1092	,	1	),
(	15	,	1093	,	1	),
(	47	,	1094	,	1	),
(	37	,	1095	,	1	),
(	14	,	1096	,	1	),
(	21	,	1097	,	1	),
(	33	,	1098	,	1	),
(	28	,	1099	,	1	),
(	8	,	1100	,	1	),
(	24	,	1101	,	1	),
(	30	,	1102	,	1	),
(	12	,	1103	,	1	),
(	6	,	1104	,	1	),
(	35	,	1105	,	1	),
(	17	,	1106	,	1	),
(	48	,	1107	,	1	),
(	52	,	1108	,	1	),
(	36	,	1109	,	1	),
(	3	,	1110	,	1	),
(	1	,	1111	,	1	),
(	34	,	1112	,	1	),
(	12	,	1113	,	1	),
(	41	,	1114	,	1	),
(	14	,	1115	,	1	),
(	6	,	1116	,	1	),
(	22	,	1117	,	1	),
(	27	,	1118	,	1	),
(	33	,	1119	,	1	),
(	52	,	1120	,	1	),
(	45	,	1121	,	1	),
(	15	,	1122	,	1	),
(	15	,	1123	,	1	),
(	30	,	1124	,	1	),
(	13	,	1125	,	1	),
(	6	,	1126	,	1	),
(	24	,	1127	,	1	),
(	51	,	1128	,	1	),
(	15	,	1129	,	1	),
(	14	,	1130	,	1	),
(	49	,	1131	,	1	),
(	35	,	1132	,	1	),
(	47	,	1133	,	1	),
(	22	,	1134	,	1	),
(	13	,	1135	,	1	),
(	40	,	1136	,	1	),
(	47	,	1137	,	1	),
(	31	,	1138	,	1	),
(	48	,	1139	,	1	),
(	19	,	1140	,	1	),
(	2	,	1141	,	1	),
(	35	,	1142	,	1	),
(	33	,	1143	,	1	),
(	14	,	1144	,	1	),
(	47	,	1145	,	1	),
(	46	,	1146	,	1	),
(	42	,	1147	,	1	),
(	51	,	1148	,	1	),
(	23	,	1149	,	1	),
(	39	,	1150	,	1	),
(	9	,	1151	,	1	),
(	21	,	1152	,	1	),
(	1	,	1153	,	1	),
(	48	,	1154	,	1	),
(	50	,	1155	,	1	),
(	41	,	1156	,	1	),
(	13	,	1157	,	1	),
(	34	,	1158	,	1	),
(	18	,	1159	,	1	),
(	12	,	1160	,	1	),
(	4	,	1161	,	1	),
(	9	,	1162	,	1	),
(	21	,	1163	,	1	),
(	18	,	1164	,	1	),
(	29	,	1165	,	1	),
(	40	,	1166	,	1	),
(	15	,	1167	,	1	),
(	9	,	1168	,	1	),
(	19	,	1169	,	1	),
(	15	,	1170	,	1	),
(	32	,	1171	,	1	),
(	43	,	1172	,	1	),
(	24	,	1173	,	1	),
(	50	,	1174	,	1	),
(	41	,	1175	,	1	),
(	1	,	1176	,	1	),
(	38	,	1177	,	1	),
(	19	,	1178	,	1	),
(	9	,	1179	,	1	),
(	30	,	1180	,	1	),
(	22	,	1181	,	1	),
(	27	,	1182	,	1	),
(	33	,	1183	,	1	),
(	11	,	1184	,	1	),
(	33	,	1185	,	1	),
(	2	,	1186	,	1	),
(	32	,	1187	,	1	),
(	2	,	1188	,	1	),
(	39	,	1189	,	1	),
(	49	,	1190	,	1	),
(	24	,	1191	,	1	),
(	35	,	1192	,	1	),
(	13	,	1193	,	1	),
(	52	,	1194	,	1	),
(	25	,	1195	,	1	),
(	30	,	1196	,	1	),
(	44	,	1197	,	1	),
(	16	,	1198	,	1	),
(	7	,	1199	,	1	),
(	50	,	1200	,	1	),
(	41	,	1201	,	1	),
(	32	,	1202	,	1	),
(	28	,	1203	,	1	),
(	8	,	1204	,	1	),
(	22	,	1205	,	1	),
(	38	,	1206	,	1	),
(	9	,	1207	,	1	),
(	24	,	1208	,	1	),
(	36	,	1209	,	1	),
(	22	,	1210	,	1	),
(	12	,	1211	,	1	),
(	19	,	1212	,	1	),
(	52	,	1213	,	1	),
(	23	,	1214	,	1	),
(	14	,	1215	,	1	),
(	3	,	1216	,	1	),
(	21	,	1217	,	1	),
(	41	,	1218	,	1	),
(	26	,	1219	,	1	),
(	47	,	1220	,	1	),
(	34	,	1221	,	1	),
(	49	,	1222	,	1	),
(	49	,	1223	,	1	),
(	22	,	1224	,	1	),
(	35	,	1225	,	1	),
(	23	,	1226	,	1	),
(	8	,	1227	,	1	),
(	14	,	1228	,	1	),
(	35	,	1229	,	1	),
(	36	,	1230	,	1	),
(	38	,	1231	,	1	),
(	18	,	1232	,	1	),
(	37	,	1233	,	1	),
(	17	,	1234	,	1	),
(	30	,	1235	,	1	),
(	16	,	1236	,	1	),
(	35	,	1237	,	1	),
(	13	,	1238	,	1	),
(	51	,	1239	,	1	),
(	29	,	1240	,	1	),
(	10	,	1241	,	1	),
(	13	,	1242	,	1	),
(	1	,	1243	,	1	),
(	32	,	1244	,	1	),
(	19	,	1245	,	1	),
(	43	,	1246	,	1	),
(	29	,	1247	,	1	),
(	40	,	1248	,	1	),
(	26	,	1249	,	1	),
(	43	,	1250	,	1	),
(	37	,	1251	,	1	),
(	11	,	1252	,	1	),
(	9	,	1253	,	1	),
(	17	,	1254	,	1	),
(	50	,	1255	,	1	),
(	18	,	1256	,	1	),
(	33	,	1257	,	1	),
(	33	,	1258	,	1	),
(	21	,	1259	,	1	),
(	1	,	1260	,	1	),
(	46	,	1261	,	1	),
(	5	,	1262	,	1	),
(	41	,	1263	,	1	),
(	25	,	1264	,	1	),
(	3	,	1265	,	1	),
(	42	,	1266	,	1	),
(	5	,	1267	,	1	),
(	21	,	1268	,	1	),
(	10	,	1269	,	1	),
(	37	,	1270	,	1	),
(	22	,	1271	,	1	),
(	30	,	1272	,	1	),
(	21	,	1273	,	1	),
(	44	,	1274	,	1	),
(	10	,	1275	,	1	),
(	15	,	1276	,	1	),
(	24	,	1277	,	1	),
(	39	,	1278	,	1	),
(	46	,	1279	,	1	),
(	16	,	1280	,	1	),
(	27	,	1281	,	1	),
(	40	,	1282	,	1	),
(	44	,	1283	,	1	),
(	25	,	1284	,	1	),
(	49	,	1285	,	1	),
(	16	,	1286	,	1	),
(	34	,	1287	,	1	),
(	1	,	1288	,	1	),
(	50	,	1289	,	1	),
(	11	,	1290	,	1	),
(	27	,	1291	,	1	),
(	33	,	1292	,	1	),
(	27	,	1293	,	1	),
(	22	,	1294	,	1	),
(	14	,	1295	,	1	),
(	47	,	1296	,	1	),
(	25	,	1297	,	1	),
(	31	,	1298	,	1	),
(	49	,	1299	,	1	),
(	17	,	1300	,	1	),
(	3	,	1301	,	1	),
(	11	,	1302	,	1	),
(	29	,	1303	,	1	),
(	25	,	1304	,	1	),
(	28	,	1305	,	1	),
(	29	,	1306	,	1	),
(	20	,	1307	,	1	),
(	25	,	1308	,	1	),
(	33	,	1309	,	1	),
(	35	,	1310	,	1	),
(	11	,	1311	,	1	),
(	27	,	1312	,	1	),
(	2	,	1313	,	1	),
(	36	,	1314	,	1	),
(	51	,	1315	,	1	),
(	34	,	1316	,	1	),
(	12	,	1317	,	1	),
(	47	,	1318	,	1	),
(	7	,	1319	,	1	),
(	42	,	1320	,	1	),
(	32	,	1321	,	1	),
(	17	,	1322	,	1	),
(	36	,	1323	,	1	),
(	9	,	1324	,	1	),
(	30	,	1325	,	1	),
(	19	,	1326	,	1	),
(	6	,	1327	,	1	),
(	34	,	1328	,	1	),
(	37	,	1329	,	1	),
(	34	,	1330	,	1	),
(	1	,	1331	,	1	),
(	23	,	1332	,	1	),
(	38	,	1333	,	1	),
(	38	,	1334	,	1	),
(	34	,	1335	,	1	),
(	19	,	1336	,	1	),
(	10	,	1337	,	1	),
(	12	,	1338	,	1	),
(	48	,	1339	,	1	),
(	19	,	1340	,	1	),
(	19	,	1341	,	1	),
(	46	,	1342	,	1	),
(	46	,	1343	,	1	),
(	28	,	1344	,	1	),
(	35	,	1345	,	1	),
(	10	,	1346	,	1	),
(	51	,	1347	,	1	),
(	42	,	1348	,	1	),
(	21	,	1349	,	1	),
(	28	,	1350	,	1	),
(	13	,	1351	,	1	),
(	24	,	1352	,	1	),
(	18	,	1353	,	1	),
(	2	,	1354	,	1	),
(	48	,	1355	,	1	),
(	37	,	1356	,	1	),
(	15	,	1357	,	1	),
(	20	,	1358	,	1	),
(	29	,	1359	,	1	),
(	39	,	1360	,	1	),
(	2	,	1361	,	1	),
(	9	,	1362	,	1	),
(	13	,	1363	,	1	),
(	20	,	1364	,	1	),
(	24	,	1365	,	1	),
(	27	,	1366	,	1	),
(	40	,	1367	,	1	),
(	10	,	1368	,	1	),
(	35	,	1369	,	1	),
(	34	,	1370	,	1	),
(	25	,	1371	,	1	),
(	8	,	1372	,	1	),
(	38	,	1373	,	1	),
(	29	,	1374	,	1	),
(	51	,	1375	,	1	),
(	46	,	1376	,	1	),
(	40	,	1377	,	1	),
(	2	,	1378	,	1	),
(	24	,	1379	,	1	),
(	49	,	1380	,	1	),
(	2	,	1381	,	1	),
(	34	,	1382	,	1	),
(	5	,	1383	,	1	),
(	33	,	1384	,	1	),
(	27	,	1385	,	1	),
(	3	,	1386	,	1	),
(	5	,	1387	,	1	),
(	19	,	1388	,	1	),
(	24	,	1389	,	1	),
(	9	,	1390	,	1	),
(	26	,	1391	,	1	),
(	36	,	1392	,	1	),
(	40	,	1393	,	1	),
(	51	,	1394	,	1	),
(	32	,	1395	,	1	),
(	25	,	1396	,	1	),
(	31	,	1397	,	1	),
(	42	,	1398	,	1	),
(	31	,	1399	,	1	),
(	26	,	1400	,	1	),
(	18	,	1401	,	1	),
(	14	,	1402	,	1	),
(	9	,	1403	,	1	),
(	19	,	1404	,	1	),
(	44	,	1405	,	1	),
(	3	,	1406	,	1	),
(	17	,	1407	,	1	),
(	22	,	1408	,	1	),
(	18	,	1409	,	1	),
(	47	,	1410	,	1	),
(	52	,	1411	,	1	),
(	39	,	1412	,	1	),
(	26	,	1413	,	1	),
(	8	,	1414	,	1	),
(	43	,	1415	,	1	),
(	2	,	1416	,	1	),
(	3	,	1417	,	1	),
(	39	,	1418	,	1	),
(	38	,	1419	,	1	),
(	11	,	1420	,	1	),
(	46	,	1421	,	1	),
(	8	,	1422	,	1	),
(	45	,	1423	,	1	),
(	31	,	1424	,	1	),
(	31	,	1425	,	1	),
(	17	,	1426	,	1	),
(	25	,	1427	,	1	),
(	34	,	1428	,	1	),
(	9	,	1429	,	1	),
(	29	,	1430	,	1	),
(	9	,	1431	,	1	),
(	11	,	1432	,	1	),
(	24	,	1433	,	1	),
(	11	,	1434	,	1	),
(	34	,	1435	,	1	),
(	23	,	1436	,	1	),
(	45	,	1437	,	1	),
(	47	,	1438	,	1	),
(	11	,	1439	,	1	),
(	30	,	1440	,	1	),
(	29	,	1441	,	1	),
(	19	,	1442	,	1	),
(	23	,	1443	,	1	),
(	31	,	1444	,	1	),
(	35	,	1445	,	1	),
(	6	,	1446	,	1	),
(	21	,	1447	,	1	),
(	22	,	1448	,	1	),
(	3	,	1449	,	1	),
(	20	,	1450	,	1	),
(	1	,	1451	,	1	),
(	39	,	1452	,	1	),
(	46	,	1453	,	1	),
(	26	,	1454	,	1	),
(	33	,	1455	,	1	),
(	23	,	1456	,	1	),
(	48	,	1457	,	1	),
(	51	,	1458	,	1	),
(	23	,	1459	,	1	),
(	4	,	1460	,	1	),
(	2	,	1461	,	1	),
(	29	,	1462	,	1	),
(	21	,	1463	,	1	),
(	47	,	1464	,	1	),
(	29	,	1465	,	1	),
(	9	,	1466	,	1	),
(	10	,	1467	,	1	),
(	43	,	1468	,	1	),
(	9	,	1469	,	1	),
(	29	,	1470	,	1	),
(	35	,	1471	,	1	),
(	40	,	1472	,	1	),
(	42	,	1473	,	1	),
(	47	,	1474	,	1	),
(	28	,	1475	,	1	),
(	38	,	1476	,	1	),
(	19	,	1477	,	1	),
(	29	,	1478	,	1	),
(	52	,	1479	,	1	),
(	23	,	1480	,	1	),
(	22	,	1481	,	1	),
(	10	,	1482	,	1	),
(	49	,	1483	,	1	),
(	44	,	1484	,	1	),
(	36	,	1485	,	1	),
(	44	,	1486	,	1	),
(	34	,	1487	,	1	),
(	35	,	1488	,	1	),
(	7	,	1489	,	1	),
(	50	,	1490	,	1	),
(	33	,	1491	,	1	),
(	43	,	1492	,	1	),
(	45	,	1493	,	1	),
(	29	,	1494	,	1	),
(	22	,	1495	,	1	),
(	11	,	1496	,	1	),
(	48	,	1497	,	1	),
(	24	,	1498	,	1	),
(	34	,	1499	,	1	),
(	52	,	1500	,	1	),
(	16	,	1501	,	1	),
(	51	,	1502	,	1	),
(	21	,	1503	,	1	),
(	20	,	1504	,	1	),
(	6	,	1505	,	1	),
(	27	,	1506	,	1	),
(	48	,	1507	,	1	),
(	3	,	1508	,	1	),
(	5	,	1509	,	1	),
(	16	,	1510	,	1	),
(	33	,	1511	,	1	),
(	25	,	1512	,	1	),
(	9	,	1513	,	1	),
(	7	,	1514	,	1	),
(	17	,	1515	,	1	),
(	42	,	1516	,	1	),
(	34	,	1517	,	1	),
(	31	,	1518	,	1	),
(	28	,	1519	,	1	),
(	11	,	1520	,	1	),
(	43	,	1521	,	1	),
(	13	,	1522	,	1	),
(	42	,	1523	,	1	),
(	50	,	1524	,	1	),
(	17	,	1525	,	1	),
(	46	,	1526	,	1	),
(	36	,	1527	,	1	),
(	52	,	1528	,	1	),
(	33	,	1529	,	1	),
(	14	,	1530	,	1	),
(	38	,	1531	,	1	),
(	4	,	1532	,	1	),
(	8	,	1533	,	1	),
(	34	,	1534	,	1	),
(	25	,	1535	,	1	),
(	40	,	1536	,	1	),
(	33	,	1537	,	1	),
(	20	,	1538	,	1	),
(	41	,	1539	,	1	),
(	52	,	1540	,	1	),
(	47	,	1541	,	1	),
(	11	,	1542	,	1	),
(	15	,	1543	,	1	),
(	27	,	1544	,	1	),
(	10	,	1545	,	1	),
(	22	,	1546	,	1	),
(	16	,	1547	,	1	),
(	40	,	1548	,	1	),
(	34	,	1549	,	1	),
(	52	,	1550	,	1	),
(	9	,	1551	,	1	),
(	43	,	1552	,	1	),
(	49	,	1553	,	1	),
(	45	,	1554	,	1	),
(	17	,	1555	,	1	),
(	25	,	1556	,	1	),
(	16	,	1557	,	1	),
(	8	,	1558	,	1	),
(	21	,	1559	,	1	),
(	38	,	1560	,	1	),
(	7	,	1561	,	1	),
(	18	,	1562	,	1	),
(	26	,	1563	,	1	),
(	37	,	1564	,	1	),
(	5	,	1565	,	1	),
(	1	,	1566	,	1	),
(	17	,	1567	,	1	),
(	8	,	1568	,	1	),
(	20	,	1569	,	1	),
(	17	,	1570	,	1	),
(	6	,	1571	,	1	),
(	8	,	1572	,	1	),
(	21	,	1573	,	1	),
(	12	,	1574	,	1	),
(	28	,	1575	,	1	),
(	34	,	1576	,	1	),
(	40	,	1577	,	1	),
(	29	,	1578	,	1	),
(	46	,	1579	,	1	),
(	31	,	1580	,	1	),
(	13	,	1581	,	1	),
(	21	,	1582	,	1	),
(	44	,	1583	,	1	),
(	38	,	1584	,	1	),
(	38	,	1585	,	1	),
(	46	,	1586	,	1	),
(	26	,	1587	,	1	),
(	37	,	1588	,	1	),
(	15	,	1589	,	1	),
(	36	,	1590	,	1	),
(	19	,	1591	,	1	),
(	27	,	1592	,	1	),
(	34	,	1593	,	1	),
(	3	,	1594	,	1	),
(	2	,	1595	,	1	),
(	33	,	1596	,	1	),
(	51	,	1597	,	1	),
(	18	,	1598	,	1	),
(	18	,	1599	,	1	),
(	31	,	1600	,	1	),
(	7	,	1601	,	1	),
(	12	,	1602	,	1	),
(	7	,	1603	,	1	),
(	50	,	1604	,	1	),
(	37	,	1605	,	1	),
(	44	,	1606	,	1	),
(	25	,	1607	,	1	),
(	46	,	1608	,	1	),
(	19	,	1609	,	1	),
(	29	,	1610	,	1	),
(	18	,	1611	,	1	),
(	49	,	1612	,	1	),
(	22	,	1613	,	1	),
(	3	,	1614	,	1	),
(	3	,	1615	,	1	),
(	44	,	1616	,	1	),
(	18	,	1617	,	1	),
(	32	,	1618	,	1	),
(	19	,	1619	,	1	),
(	22	,	1620	,	1	),
(	21	,	1621	,	1	),
(	34	,	1622	,	1	),
(	40	,	1623	,	1	),
(	22	,	1624	,	1	),
(	31	,	1625	,	1	),
(	32	,	1626	,	1	),
(	36	,	1627	,	1	),
(	43	,	1628	,	1	),
(	5	,	1629	,	1	),
(	7	,	1630	,	1	),
(	35	,	1631	,	1	),
(	33	,	1632	,	1	),
(	4	,	1633	,	1	),
(	7	,	1634	,	1	),
(	51	,	1635	,	1	),
(	34	,	1636	,	1	),
(	14	,	1637	,	1	),
(	14	,	1638	,	1	),
(	26	,	1639	,	1	),
(	45	,	1640	,	1	),
(	12	,	1641	,	1	),
(	35	,	1642	,	1	),
(	9	,	1643	,	1	),
(	14	,	1644	,	1	),
(	39	,	1645	,	1	),
(	51	,	1646	,	1	),
(	42	,	1647	,	1	),
(	45	,	1648	,	1	),
(	26	,	1649	,	1	),
(	3	,	1650	,	1	),
(	30	,	1651	,	1	),
(	45	,	1652	,	1	),
(	48	,	1653	,	1	),
(	36	,	1654	,	1	),
(	32	,	1655	,	1	),
(	49	,	1656	,	1	),
(	42	,	1657	,	1	),
(	2	,	1658	,	1	),
(	27	,	1659	,	1	),
(	35	,	1660	,	1	),
(	6	,	1661	,	1	),
(	30	,	1662	,	1	),
(	19	,	1663	,	1	),
(	24	,	1664	,	1	),
(	32	,	1665	,	1	),
(	31	,	1666	,	1	),
(	14	,	1667	,	1	),
(	51	,	1668	,	1	),
(	38	,	1669	,	1	),
(	6	,	1670	,	1	),
(	16	,	1671	,	1	),
(	19	,	1672	,	1	),
(	32	,	1673	,	1	),
(	19	,	1674	,	1	),
(	4	,	1675	,	1	),
(	34	,	1676	,	1	),
(	29	,	1677	,	1	),
(	19	,	1678	,	1	),
(	5	,	1679	,	1	),
(	20	,	1680	,	1	),
(	25	,	1681	,	1	),
(	31	,	1682	,	1	),
(	22	,	1683	,	1	),
(	14	,	1684	,	1	),
(	5	,	1685	,	1	),
(	23	,	1686	,	1	),
(	14	,	1687	,	1	),
(	44	,	1688	,	1	),
(	37	,	1689	,	1	),
(	10	,	1690	,	1	),
(	15	,	1691	,	1	),
(	28	,	1692	,	1	),
(	37	,	1693	,	1	),
(	10	,	1694	,	1	),
(	2	,	1695	,	1	),
(	38	,	1696	,	1	),
(	48	,	1697	,	1	),
(	44	,	1698	,	1	),
(	47	,	1699	,	1	),
(	28	,	1700	,	1	),
(	10	,	1701	,	1	),
(	47	,	1702	,	1	),
(	50	,	1703	,	1	),
(	3	,	1704	,	1	),
(	24	,	1705	,	1	),
(	1	,	1706	,	1	),
(	43	,	1707	,	1	),
(	2	,	1708	,	1	),
(	22	,	1709	,	1	),
(	35	,	1710	,	1	),
(	6	,	1711	,	1	),
(	48	,	1712	,	1	),
(	39	,	1713	,	1	),
(	22	,	1714	,	1	),
(	52	,	1715	,	1	),
(	31	,	1716	,	1	),
(	26	,	1717	,	1	),
(	8	,	1718	,	1	),
(	10	,	1719	,	1	),
(	5	,	1720	,	1	),
(	29	,	1721	,	1	),
(	8	,	1722	,	1	),
(	39	,	1723	,	1	),
(	17	,	1724	,	1	),
(	39	,	1725	,	1	),
(	45	,	1726	,	1	),
(	3	,	1727	,	1	),
(	17	,	1728	,	1	),
(	16	,	1729	,	1	),
(	31	,	1730	,	1	),
(	47	,	1731	,	1	),
(	41	,	1732	,	1	),
(	4	,	1733	,	1	),
(	22	,	1734	,	1	),
(	8	,	1735	,	1	),
(	44	,	1736	,	1	),
(	22	,	1737	,	1	),
(	32	,	1738	,	1	),
(	38	,	1739	,	1	),
(	12	,	1740	,	1	),
(	24	,	1741	,	1	),
(	36	,	1742	,	1	),
(	3	,	1743	,	1	),
(	28	,	1744	,	1	),
(	45	,	1745	,	1	),
(	37	,	1746	,	1	),
(	44	,	1747	,	1	),
(	34	,	1748	,	1	),
(	35	,	1749	,	1	),
(	47	,	1750	,	1	),
(	3	,	1751	,	1	),
(	4	,	1752	,	1	),
(	3	,	1753	,	1	),
(	6	,	1754	,	1	),
(	39	,	1755	,	1	),
(	27	,	1756	,	1	),
(	22	,	1757	,	1	),
(	3	,	1758	,	1	),
(	25	,	1759	,	1	),
(	7	,	1760	,	1	),
(	48	,	1761	,	1	),
(	3	,	1762	,	1	),
(	26	,	1763	,	1	),
(	32	,	1764	,	1	),
(	50	,	1765	,	1	),
(	22	,	1766	,	1	),
(	38	,	1767	,	1	),
(	16	,	1768	,	1	),
(	20	,	1769	,	1	),
(	42	,	1770	,	1	),
(	28	,	1771	,	1	),
(	18	,	1772	,	1	),
(	23	,	1773	,	1	),
(	21	,	1774	,	1	),
(	34	,	1775	,	1	),
(	3	,	1776	,	1	),
(	10	,	1777	,	1	),
(	32	,	1778	,	1	),
(	6	,	1779	,	1	),
(	26	,	1780	,	1	),
(	12	,	1781	,	1	),
(	29	,	1782	,	1	),
(	27	,	1783	,	1	),
(	6	,	1784	,	1	),
(	38	,	1785	,	1	),
(	27	,	1786	,	1	),
(	49	,	1787	,	1	),
(	6	,	1788	,	1	),
(	52	,	1789	,	1	),
(	18	,	1790	,	1	),
(	42	,	1791	,	1	),
(	17	,	1792	,	1	),
(	47	,	1793	,	1	),
(	40	,	1794	,	1	),
(	9	,	1795	,	1	),
(	21	,	1796	,	1	),
(	13	,	1797	,	1	),
(	9	,	1798	,	1	),
(	10	,	1799	,	1	),
(	38	,	1800	,	1	),
(	34	,	1801	,	1	),
(	51	,	1802	,	1	),
(	14	,	1803	,	1	),
(	17	,	1804	,	1	),
(	38	,	1805	,	1	),
(	48	,	1806	,	1	),
(	1	,	1807	,	1	),
(	44	,	1808	,	1	),
(	3	,	1809	,	1	),
(	20	,	1810	,	1	),
(	20	,	1811	,	1	),
(	44	,	1812	,	1	),
(	31	,	1813	,	1	),
(	38	,	1814	,	1	),
(	37	,	1815	,	1	),
(	11	,	1816	,	1	),
(	27	,	1817	,	1	),
(	38	,	1818	,	1	),
(	19	,	1819	,	1	),
(	49	,	1820	,	1	),
(	33	,	1821	,	1	),
(	46	,	1822	,	1	),
(	37	,	1823	,	1	),
(	10	,	1824	,	1	),
(	50	,	1825	,	1	),
(	12	,	1826	,	1	),
(	3	,	1827	,	1	),
(	14	,	1828	,	1	),
(	18	,	1829	,	1	),
(	51	,	1830	,	1	),
(	18	,	1831	,	1	),
(	14	,	1832	,	1	),
(	33	,	1833	,	1	),
(	41	,	1834	,	1	),
(	2	,	1835	,	1	),
(	15	,	1836	,	1	),
(	44	,	1837	,	1	),
(	41	,	1838	,	1	),
(	9	,	1839	,	1	),
(	13	,	1840	,	1	),
(	22	,	1841	,	1	),
(	41	,	1842	,	1	),
(	31	,	1843	,	1	),
(	35	,	1844	,	1	),
(	33	,	1845	,	1	),
(	8	,	1846	,	1	),
(	34	,	1847	,	1	),
(	29	,	1848	,	1	),
(	48	,	1849	,	1	),
(	21	,	1850	,	1	),
(	45	,	1851	,	1	),
(	16	,	1852	,	1	),
(	14	,	1853	,	1	),
(	7	,	1854	,	1	),
(	37	,	1855	,	1	),
(	10	,	1856	,	1	),
(	50	,	1857	,	1	),
(	48	,	1858	,	1	),
(	43	,	1859	,	1	),
(	23	,	1860	,	1	),
(	21	,	1861	,	1	),
(	36	,	1862	,	1	),
(	44	,	1863	,	1	),
(	31	,	1864	,	1	),
(	48	,	1865	,	1	),
(	50	,	1866	,	1	),
(	36	,	1867	,	1	),
(	17	,	1868	,	1	),
(	8	,	1869	,	1	),
(	24	,	1870	,	1	),
(	27	,	1871	,	1	),
(	41	,	1872	,	1	),
(	48	,	1873	,	1	),
(	51	,	1874	,	1	),
(	15	,	1875	,	1	),
(	7	,	1876	,	1	),
(	1	,	1877	,	1	),
(	47	,	1878	,	1	),
(	10	,	1879	,	1	),
(	27	,	1880	,	1	),
(	28	,	1881	,	1	),
(	36	,	1882	,	1	),
(	30	,	1883	,	1	),
(	37	,	1884	,	1	),
(	31	,	1885	,	1	),
(	37	,	1886	,	1	),
(	5	,	1887	,	1	),
(	27	,	1888	,	1	),
(	49	,	1889	,	1	),
(	36	,	1890	,	1	),
(	5	,	1891	,	1	),
(	52	,	1892	,	1	),
(	30	,	1893	,	1	),
(	47	,	1894	,	1	),
(	22	,	1895	,	1	),
(	37	,	1896	,	1	),
(	48	,	1897	,	1	),
(	18	,	1898	,	1	),
(	23	,	1899	,	1	),
(	49	,	1900	,	1	),
(	18	,	1901	,	1	),
(	51	,	1902	,	1	),
(	43	,	1903	,	1	),
(	28	,	1904	,	1	),
(	34	,	1905	,	1	),
(	23	,	1906	,	1	),
(	21	,	1907	,	1	),
(	13	,	1908	,	1	),
(	37	,	1909	,	1	),
(	31	,	1910	,	1	),
(	33	,	1911	,	1	),
(	38	,	1912	,	1	),
(	33	,	1913	,	1	),
(	7	,	1914	,	1	),
(	9	,	1915	,	1	),
(	40	,	1916	,	1	),
(	12	,	1917	,	1	),
(	24	,	1918	,	1	),
(	48	,	1919	,	1	),
(	51	,	1920	,	1	),
(	26	,	1921	,	1	),
(	22	,	1922	,	1	),
(	50	,	1923	,	1	),
(	27	,	1924	,	1	),
(	14	,	1925	,	1	),
(	2	,	1926	,	1	),
(	15	,	1927	,	1	),
(	10	,	1928	,	1	),
(	9	,	1929	,	1	),
(	20	,	1930	,	1	),
(	13	,	1931	,	1	),
(	22	,	1932	,	1	),
(	50	,	1933	,	1	),
(	23	,	1934	,	1	),
(	4	,	1935	,	1	),
(	1	,	1936	,	1	),
(	4	,	1937	,	1	),
(	24	,	1938	,	1	),
(	47	,	1939	,	1	),
(	9	,	1940	,	1	),
(	28	,	1941	,	1	),
(	38	,	1942	,	1	),
(	4	,	1943	,	1	),
(	13	,	1944	,	1	),
(	19	,	1945	,	1	),
(	52	,	1946	,	1	),
(	45	,	1947	,	1	),
(	48	,	1948	,	1	),
(	18	,	1949	,	1	),
(	2	,	1950	,	1	),
(	32	,	1951	,	1	),
(	31	,	1952	,	1	),
(	34	,	1953	,	1	),
(	45	,	1954	,	1	),
(	17	,	1955	,	1	),
(	50	,	1956	,	1	),
(	26	,	1957	,	1	),
(	8	,	1958	,	1	),
(	38	,	1959	,	1	),
(	14	,	1960	,	1	),
(	39	,	1961	,	1	),
(	19	,	1962	,	1	),
(	7	,	1963	,	1	),
(	14	,	1964	,	1	),
(	27	,	1965	,	1	),
(	48	,	1966	,	1	),
(	2	,	1967	,	1	),
(	18	,	1968	,	1	),
(	36	,	1969	,	1	),
(	38	,	1970	,	1	),
(	31	,	1971	,	1	),
(	12	,	1972	,	1	),
(	43	,	1973	,	1	),
(	2	,	1974	,	1	),
(	21	,	1975	,	1	),
(	16	,	1976	,	1	),
(	40	,	1977	,	1	),
(	27	,	1978	,	1	),
(	42	,	1979	,	1	),
(	35	,	1980	,	1	),
(	6	,	1981	,	1	),
(	30	,	1982	,	1	),
(	44	,	1983	,	1	),
(	35	,	1984	,	1	),
(	26	,	1985	,	1	),
(	19	,	1986	,	1	),
(	46	,	1987	,	1	),
(	26	,	1988	,	1	),
(	16	,	1989	,	1	),
(	30	,	1990	,	1	),
(	22	,	1991	,	1	),
(	14	,	1992	,	1	),
(	3	,	1993	,	1	),
(	22	,	1994	,	1	),
(	42	,	1995	,	1	),
(	52	,	1996	,	1	),
(	28	,	1997	,	1	),
(	25	,	1998	,	1	),
(	52	,	1999	,	1	),
(	17	,	2000	,	1	),
(	4	,	2001	,	1	),
(	40	,	2002	,	1	),
(	33	,	2003	,	1	),
(	31	,	2004	,	1	),
(	30	,	2005	,	1	),
(	7	,	2006	,	1	),
(	27	,	2007	,	1	),
(	27	,	2008	,	1	),
(	42	,	2009	,	1	),
(	46	,	2010	,	1	),
(	34	,	2011	,	1	),
(	6	,	2012	,	1	),
(	1	,	2013	,	1	),
(	10	,	2014	,	1	),
(	19	,	2015	,	1	),
(	37	,	2016	,	1	),
(	49	,	2017	,	1	),
(	36	,	2018	,	1	),
(	10	,	2019	,	1	),
(	26	,	2020	,	1	),
(	17	,	2021	,	1	),
(	18	,	2022	,	1	),
(	7	,	2023	,	1	),
(	24	,	2024	,	1	),
(	52	,	2025	,	1	),
(	24	,	2026	,	1	),
(	35	,	2027	,	1	),
(	1	,	2028	,	1	),
(	29	,	2029	,	1	),
(	40	,	2030	,	1	),
(	34	,	2031	,	1	),
(	19	,	2032	,	1	),
(	41	,	2033	,	1	),
(	49	,	2034	,	1	),
(	16	,	2035	,	1	),
(	36	,	2036	,	1	),
(	47	,	2037	,	1	),
(	36	,	2038	,	1	),
(	2	,	2039	,	1	),
(	16	,	2040	,	1	),
(	36	,	2041	,	1	),
(	17	,	2042	,	1	),
(	18	,	2043	,	1	),
(	15	,	2044	,	1	),
(	45	,	2045	,	1	),
(	4	,	2046	,	1	),
(	8	,	2047	,	1	),
(	7	,	2048	,	1	),
(	11	,	2049	,	1	),
(	4	,	2050	,	1	),
(	30	,	2051	,	1	),
(	2	,	2052	,	1	),
(	19	,	2053	,	1	),
(	10	,	2054	,	1	),
(	10	,	2055	,	1	),
(	52	,	2056	,	1	),
(	41	,	2057	,	1	),
(	35	,	2058	,	1	),
(	1	,	2059	,	1	),
(	31	,	2060	,	1	),
(	1	,	2061	,	1	),
(	38	,	2062	,	1	),
(	44	,	2063	,	1	),
(	45	,	2064	,	1	),
(	35	,	2065	,	1	),
(	51	,	2066	,	1	),
(	50	,	2067	,	1	),
(	7	,	2068	,	1	),
(	27	,	2069	,	1	),
(	39	,	2070	,	1	),
(	22	,	2071	,	1	),
(	12	,	2072	,	1	),
(	23	,	2073	,	1	),
(	13	,	2074	,	1	),
(	43	,	2075	,	1	),
(	33	,	2076	,	1	),
(	37	,	2077	,	1	),
(	24	,	2078	,	1	),
(	6	,	2079	,	1	),
(	41	,	2080	,	1	),
(	5	,	2081	,	1	),
(	47	,	2082	,	1	),
(	18	,	2083	,	1	),
(	46	,	2084	,	1	),
(	51	,	2085	,	1	),
(	14	,	2086	,	1	),
(	6	,	2087	,	1	),
(	16	,	2088	,	1	),
(	17	,	2089	,	1	),
(	8	,	2090	,	1	),
(	40	,	2091	,	1	),
(	30	,	2092	,	1	),
(	42	,	2093	,	1	),
(	20	,	2094	,	1	),
(	51	,	2095	,	1	),
(	52	,	2096	,	1	),
(	5	,	2097	,	1	),
(	31	,	2098	,	1	),
(	3	,	2099	,	1	),
(	2	,	2100	,	1	),
(	11	,	2101	,	1	),
(	46	,	2102	,	1	),
(	23	,	2103	,	1	),
(	25	,	2104	,	1	),
(	39	,	2105	,	1	),
(	8	,	2106	,	1	),
(	49	,	2107	,	1	),
(	3	,	2108	,	1	),
(	30	,	2109	,	1	),
(	18	,	2110	,	1	),
(	15	,	2111	,	1	),
(	22	,	2112	,	1	),
(	52	,	2113	,	1	),
(	5	,	2114	,	1	),
(	6	,	2115	,	1	),
(	13	,	2116	,	1	),
(	15	,	2117	,	1	),
(	33	,	2118	,	1	),
(	1	,	2119	,	1	),
(	30	,	2120	,	1	),
(	37	,	2121	,	1	),
(	39	,	2122	,	1	),
(	47	,	2123	,	1	),
(	19	,	2124	,	1	),
(	44	,	2125	,	1	),
(	5	,	2126	,	1	),
(	7	,	2127	,	1	),
(	8	,	2128	,	1	),
(	24	,	2129	,	1	),
(	20	,	2130	,	1	),
(	49	,	2131	,	1	),
(	7	,	2132	,	1	),
(	43	,	2133	,	1	),
(	13	,	2134	,	1	),
(	14	,	2135	,	1	),
(	18	,	2136	,	1	),
(	43	,	2137	,	1	),
(	46	,	2138	,	1	),
(	21	,	2139	,	1	),
(	9	,	2140	,	1	),
(	38	,	2141	,	1	),
(	51	,	2142	,	1	),
(	51	,	2143	,	1	),
(	3	,	2144	,	1	),
(	19	,	2145	,	1	),
(	47	,	2146	,	1	),
(	43	,	2147	,	1	),
(	46	,	2148	,	1	),
(	48	,	2149	,	1	),
(	15	,	2150	,	1	),
(	37	,	2151	,	1	),
(	34	,	2152	,	1	),
(	3	,	2153	,	1	),
(	13	,	2154	,	1	),
(	28	,	2155	,	1	),
(	11	,	2156	,	1	),
(	36	,	2157	,	1	),
(	47	,	2158	,	1	),
(	37	,	2159	,	1	),
(	40	,	2160	,	1	),
(	12	,	2161	,	1	),
(	51	,	2162	,	1	),
(	38	,	2163	,	1	),
(	23	,	2164	,	1	),
(	32	,	2165	,	1	),
(	43	,	2166	,	1	),
(	27	,	2167	,	1	);
