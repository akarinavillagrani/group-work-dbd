--Persona natural
--Todas las acciones que tiene un usuario que desea vender, con descripcion completa
select u.rut 
, u.nombre 
, u.apellido 
, o.fecha_transaccion 
, o.nro_orden 
, o.precio_accion 
,  to2.descripcion 
, do2.accion_codigo_accion 
, a2.descripcion 
, em2.razon_social 
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --pendiente
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut ='24812644-2'
and to2.descripcion ='V'
order by o.nro_orden , em2.rut 

--Monto deseado a recaudar en todas las ventas de sus acciones 
select SUM(o.precio_accion)
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  in (1,2)--pendiente y realizadas
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut ='24812644-2'
and to2.descripcion ='V'
 
--Todas las acciones que tiene un usuario que desea comprar
select u.rut 
, u.nombre 
, u.apellido 
, o.fecha_transaccion 
, o.nro_orden 
, o.precio_accion 
,  to2.descripcion 
, do2.accion_codigo_accion 
, a2.descripcion 
, em2.razon_social 
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --pendiente
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut ='24812644-2'
and to2.descripcion ='C'
order by o.nro_orden , em2.rut 

--totales de valor de compra y valores de venta de acciones del usuario
select SUM(o.precio_accion), 'COMPRA'
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut ='24812644-2'
and to2.descripcion ='C'
union all 
select SUM(o.precio_accion), 'VENTA'
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut ='24812644-2'
and to2.descripcion ='V'



/*Match de las acciones que */
select * from 
(
select to2.descripcion
	,o.usuario_rut 
	, o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut <> '24812644-2'
and to2.descripcion ='V'
order by do2.accion_codigo_accion , o.nro_orden , em2.rut ) as VENTA
union ALL
select * from 
(
	select 
	to2.descripcion
	, o.usuario_rut 
	,o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
	from usuario u
	inner join orden o on o.usuario_rut =u.rut 
	inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
	inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
	inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
	inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
	inner join empresa em2 on em2.rut  = a2.empresa_rut 
	where u.rut ='24812644-2'
	and to2.descripcion ='C'
order by do2.accion_codigo_accion ,o.nro_orden , em2.rut 
) as COMPRA


/*Match donde el usuario 24812644-2 cinciden el precio de venta a lo que el ofrece */
--331
select distinct(VENTA.descripcion) accion
	, VENTA.razon_social 
	--COMPRA.usuario_rut rut_compra
	,VENTA.usuario_rut rut_vende
	--,COMPRA.tipo_operacion tipo_Operacion_compra
	--,VENTA.tipo_operacion tipo_Orden_Venta
	,COMPRA.precio_accion precio_accion_compra
	,VENTA.precio_accion precio_accion_Venta
from 
(
select to2.descripcion tipo_operacion
	,o.usuario_rut 
	, o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
	, em2.rut 
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut <> '24812644-2'
and to2.descripcion ='V'
order by do2.accion_codigo_accion , o.nro_orden , em2.rut ) as VENTA
inner join 
(
	select to2.descripcion tipo_operacion
	, o.usuario_rut 
	, o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
	, em2.rut 
	from usuario u
	inner join orden o on o.usuario_rut =u.rut 
	inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
	inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
	inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
	inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
	inner join empresa em2 on em2.rut  = a2.empresa_rut 
	where u.rut ='24812644-2'
	and to2.descripcion ='C'
order by do2.accion_codigo_accion ,o.nro_orden , em2.rut 
) as COMPRA on COMPRA.rut =VENTA.rut and COMPRA.precio_accion >= VENTA.precio_accion



/*Match donde el usuario 24812644-2 el precio de venta es mejor al pecio de compra */
select distinct(COMPRA.descripcion) accion
	, VENTA.razon_social 
	,VENTA.usuario_rut rut_vende
	,COMPRA.precio_accion precio_accion_compra
	,VENTA.precio_accion precio_accion_Venta
from 
(
select to2.descripcion tipo_operacion
	,o.usuario_rut 
	, o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
	, em2.rut 
from usuario u
inner join orden o on o.usuario_rut =u.rut 
inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
inner join empresa em2 on em2.rut  = a2.empresa_rut 
where u.rut = '24812644-2'
and to2.descripcion ='V'
order by do2.accion_codigo_accion , o.nro_orden , em2.rut ) as VENTA
inner join 
(
	select to2.descripcion tipo_operacion
	, o.usuario_rut 
	, o.precio_accion 
	, do2.accion_codigo_accion 
	, a2.descripcion 
	, em2.razon_social 
	, em2.rut 
	from usuario u
	inner join orden o on o.usuario_rut =u.rut 
	inner join tipo_orden to2 on to2.orden_nro_orden  = o.nro_orden 
	inner join detalle_orden do2 on do2.orden_nro_orden = o.nro_orden 
	inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 1 --Pendiente
	inner join accion a2 on a2.codigo_accion  = do2.accion_codigo_accion 
	inner join empresa em2 on em2.rut  = a2.empresa_rut 
	where u.rut <>'24812644-2'
	and to2.descripcion ='C'
order by do2.accion_codigo_accion ,o.nro_orden , em2.rut 
) as COMPRA on COMPRA.rut =VENTA.rut and COMPRA.precio_accion >= VENTA.precio_accion



--Corredora de bolsa.
/*Información de los usuarios que tiene el sistema y los potenciales que aún no se encuentran verificados o activos*/
select count(*)
	, es_activo
	,es_verificado
from usuario 
group by es_activo
	,es_verificado
	
	
--Analista de negocio de la corredora
select  * from orden o 
order by fecha_transaccion desc
FETCH FIRST 5 ROWS only

--Top 5 Acciones más transaccionadas del dia
select count(*)
	,e2.razon_social  
	from detalle_orden do2 
	inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
	inner join accion a2 on a2.codigo_accion =do2.accion_codigo_accion 
	inner join empresa e2 on e2.rut  = a2.empresa_rut 
where orden_nro_orden in (
							select nro_orden
								from orden o 
								--where fecha_transaccion >=NOW() --comentada 
							order by fecha_transaccion desc
							FETCH FIRST 5 ROWS only)
group by e2.razon_social  
order by count(*) desc
FETCH FIRST 5 ROWS only


--Top 5 Acciones de menor valor del dia
select AVG(o.precio_accion) 
	, e2.razon_social
from orden o 
inner join detalle_orden do2 on do2.orden_nro_orden =o.nro_orden
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
inner join accion a2 on a2.codigo_accion =do2.accion_codigo_accion 
	inner join empresa e2 on e2.rut  = a2.empresa_rut 
--where fecha_transaccion >=NOW() --comentada no hay transacciones tan cercanas
group by  e2.razon_social 
	, o.precio_accion
order by o.precio_accion asc
FETCH FIRST 5 ROWS only


--Establecer precio mercado para una acción
select e.razon_social
	, AVG(o.precio_accion) precio
from detalle_orden do2 
	inner join orden o on o.nro_orden = do2.orden_nro_orden
	inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
	inner join accion a ON a.codigo_accion = do2.accion_codigo_accion 
	inner join empresa e on e.rut = a.empresa_rut 
group by e.razon_social

--Listado de acciones en venta pendiente
select 	 *
from orden o2 
	inner join tipo_orden to2 on to2.orden_nro_orden = o2.nro_orden and to2.descripcion ='V'
	inner join detalle_orden do2 on do2.orden_nro_orden  = o2.nro_orden 
	inner join tipo_estado te on te.id  = do2.tipo_estado_id and te.id = 1--Pendiente
order by do2.accion_codigo_accion 

--Sysadmin
--Parametros de la Base de datos
select name, setting, boot_val, reset_val, unit
from pg_settings
order by name; 

--número de transacciones sin vaciar
SELECT datname, age(datfrozenxid) FROM pg_database ORDER BY age(datfrozenxid) desc limit 20;

-- lista de tablas que autovacuum considera aptas para el vacío
 WITH vbt AS (SELECT setting AS autovacuum_vacuum_threshold FROM 
pg_settings WHERE name = 'autovacuum_vacuum_threshold')
    , vsf AS (SELECT setting AS autovacuum_vacuum_scale_factor FROM 
pg_settings WHERE name = 'autovacuum_vacuum_scale_factor')
    , fma AS (SELECT setting AS autovacuum_freeze_max_age FROM 
pg_settings WHERE name = 'autovacuum_freeze_max_age')
    , sto AS (select opt_oid, split_part(setting, '=', 1) as param, 
split_part(setting, '=', 2) as value from (select oid opt_oid, 
unnest(reloptions) setting from pg_class) opt)
SELECT
    '"'||ns.nspname||'"."'||c.relname||'"' as relation
    , pg_size_pretty(pg_table_size(c.oid)) as table_size
    , age(relfrozenxid) as xid_age
    , coalesce(cfma.value::float, autovacuum_freeze_max_age::float) 
autovacuum_freeze_max_age
    , (coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) 
+ coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * 
c.reltuples) as autovacuum_vacuum_tuples
    , n_dead_tup as dead_tuples
FROM pg_class c join pg_namespace ns on ns.oid = c.relnamespace
join pg_stat_all_tables stat on stat.relid = c.oid
join vbt on (1=1) join vsf on (1=1) join fma on (1=1)
left join sto cvbt on cvbt.param = 'autovacuum_vacuum_threshold' and 
c.oid = cvbt.opt_oid
left join sto cvsf on cvsf.param = 'autovacuum_vacuum_scale_factor' and 
c.oid = cvsf.opt_oid
left join sto cfma on cfma.param = 'autovacuum_freeze_max_age' and 
c.oid = cfma.opt_oid
WHERE c.relkind = 'r' and nspname <> 'pg_catalog'
and (
    age(relfrozenxid) >= coalesce(cfma.value::float, 
autovacuum_freeze_max_age::float)
    or
    coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) + 
coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * 
c.reltuples <= n_dead_tup
   -- or 1 = 1
)
ORDER BY age(relfrozenxid) DESC LIMIT 50;


--reportes de negocio de compra y venta de acciones identificando empresa, monto en pesos
select SUM(o.precio_accion) total
	,o.nro_orden 
	, u2.rut 
	,u2.nombre 
	,CASE WHEN to2.descripcion ='C' THEN 'Compra'
            WHEN to2.descripcion ='V' THEN 'Venta'
            ELSE 'Desconocido'
       END 
from orden o 
inner join detalle_orden do2 on do2.orden_nro_orden =o.nro_orden 
inner join tipo_estado te on te.id = do2.tipo_estado_id and te.id  = 2 --realizada
inner join usuario u2 on u2.rut = o.usuario_rut 
inner join tipo_orden to2 on to2.orden_nro_orden = o.nro_orden 
group by o.nro_orden
	, u2.rut 
	,u2.nombre 
	,to2.descripcion 

	, u2.rut 
	,u2.nombre 
	,to2.descripcion 
