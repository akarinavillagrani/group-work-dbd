CREATE INDEX index_detalle_orden ON detalle_orden (id_detalle_orden, 
													orden_nro_orden, 
													accion_codigo_accion,
													tipo_estado_id);
													
CREATE INDEX index_orden ON orden (nro_orden, usuario_rut);

CREATE INDEX index_tipo_orden ON tipo_orden (id, orden_nro_orden);

