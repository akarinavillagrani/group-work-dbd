INSERT INTO TIPO_USUARIO (id, descripcion)
VALUES (564, 'descripcion_TIPO_USUARIO1'),
(218, 'descripcion_TIPO_USUARIO2'),
(8723, 'descripcion_TIPO_USUARIO3'),
(1, 'descripcion_TIPO_USUARIO4'),
(716, 'descripcion_TIPO_USUARIO5');

INSERT INTO USUARIO (rut, nombre, apellido, email, direccion, telefono, sexo, es_activo, es_verificado, nacionalidad, fecha_creacion_usu, nro_serie, carnet_foto, tipo_usuario)
VALUES ('11111111-1', 'Juan', 'Brown', 'mail1@gmail.com', 'calle falsa 124', 11111111, 'masculino', true, true, 'chileno', '2020-11-18 13:07:34', 'num_serie1', B'10'::bit(1000), 564),
('2222222-2', 'Red', 'Tube', 'mail2@gmail.com', 'calle verdadera 1234', 2222222, 'femenino', true, false, 'peruano', '2020-03-26 04:05:26', 'num_serie2', B'10'::bit(1000), 564),
('33333333-3', 'Mila', 'Gros de Abril', 'mail3@gmail.com', 'calle nosellama 545', 3333333, 'trans', false, true, 'koreano', '2020-06-11 13:05:16', 'num_serie3', B'10'::bit(1000), 1),
('4444444-4', 'Dolores', 'Quijada', 'mail4@gmail.com', 'real calle 12', 4444444, 'femenino', true, false, 'venezolano', '2020-03-26 04:05:26', 'num_serie4', B'10'::bit(1000), 1),
('5555555-5', 'Venti', 'Ramos Perez', 'mail5@gmail.com', 'av brasil 1984', 5555555, 'undefined', true, false, 'peruano', '2020-03-26 04:05:26', 'num_serie5', B'10'::bit(1000), 716);
('6666666-6', 'Lienlaf', 'Irma Gloria', 'mail6@gmail.com', 'Mayor ABE 4007', 66666666, 'undefined', true, false, 'chilena', '2020-04-26 04:05:26', 'num_serie6', B'10'::bit(1000), 716);

INSERT INTO TIPO_TRANSACCION (descripcion, id)
VALUES ('descripcion_transaccion1', 234),
('descripcion_transaccion2', 123),
('descripcion_transaccion3', 871),
('descripcion_transaccion4', 3),
('descripcion_transaccion5', 112);
('descripcion_transaccion5', 114);

INSERT INTO CARTOLA_CUENTA (nro_operacion, fecha_transaccion, monto, USUARIO_rut, TIPO_TRANSACCION_id)
VALUES (156, '2020-11-10 14:05:55', 12331, '5555555-5', 123),
(45, '2020-07-12 18:15:55', 564324, '4444444-4', 123),
(545, '2020-09-22 19:12:25', 9872, '33333333-3', 3),
(75, '2020-11-14 18:15:07', 45, '33333333-3', 123),
(2, '2020-03-21 10:33:55', 653244, '2222222-2', 112);
(3, '2020-03-21 10:33:55', 653244, '6666666-6', 114);

INSERT INTO TIPO_CUENTA (id, descripcion)
VALUES (654, 'descripcion_cuenta1'),
(87, 'descripcion_cuenta2'),
(1, 'descripcion_cuenta3'),
(34, 'descripcion_cuenta4'),
(23, 'descripcion_cuenta5');
(23, 'descripcion_cuenta6');


INSERT INTO CUENTA_BANCARIA (nro_cuenta, banco, USUARIO_rut, TIPO_CUENTA_id)
VALUES ('154545245', 'banco1','11111111-1',34),
('654548745', 'banco1','33333333-3', 23),
('362544234', 'banco2', '33333333-3', 87),
('543412687', 'banco1', '2222222-2', 1),
('873545222', 'banco2', '4444444-4', 23);
('571311002', 'banco6', '6666666-6', 23);


INSERT INTO CONTRATO (direccion, fecha_contratacion, fecha_finalizacion, esta_firmado, terminos_y_condiciones, comision, pdf, rut_emisor_contrato)
VALUES ('direccion_contrato1', '2019-01-10 08:15:06', '2020-01-10 08:15:06', true, 'terminos y contratos1', 324346, B'10'::bit(1000),'33333333-3'),
('direccion_contrato2', '2019-02-10 18:15:06', '2020-06-10 11:15:06', false, 'terminos y contratos2', 987235, B'10'::bit(1000), '33333333-3'),
('direccion_contrato3', '2019-12-10 13:25:06', '2020-12-10 12:15:06', false, 'terminos y contratos3', 543645, B'10'::bit(1000), '11111111-1'),
('direccion_contrato4', '2019-07-10 04:16:06', '2020-04-10 09:15:06', false, 'terminos y contratos4', 13234, B'10'::bit(1000), '2222222-2'),
('direccion_contrato5', '2019-06-10 11:22:06', '2020-06-10 02:15:06', true, 'terminos y contratos5', 345455541, B'10'::bit(1000), '4444444-4');
('direccion_contrato6', '2019-06-10 11:22:06', '2020-06-10 02:15:06', true, 'terminos y contratos6', 345455541, B'10'::bit(1000), '6666666-6');

INSERT INTO  FACTURA (nro_factura, monto_comision, fecha_facturacion, USUARIO_rut)
VALUES (54, 6527864, '2020-10-11 08:15:06','4444444-4'),
(3451, 5643451, '2020-01-15 18:15:39', '33333333-3'),
(2354, 5643451, '2020-02-19 18:15:07','33333333-3'),
(121, 5643451, '2020-12-15 18:15:26', '2222222-2'),
(4, 982374, '2020-04-12 08:15:45', '2222222-2');

INSERT INTO ORDEN (nro_orden, precio_accion, fecha_transaccion, USUARIO_rut)
VALUES (1, 5612, '2020-01-08 04:05:06', '2222222-2'),
(3, 2345, '2020-01-09 04:05:06', '33333333-3'),
(17, 65725, '2020-03-10 04:05:06', '4444444-4'),
(41, 78747, '2020-06-10 14:05:06', '2222222-2'),
(121, 23, '2020-12-10 04:25:06', '33333333-3');

INSERT INTO TIPO_ORDEN (id, descripcion, ORDEN_nro_orden)
VALUES (6531, 'descripcio_tipo_orden1', 3),
(344, 'descripcio_tipo_orden2', 41),
(8712, 'descripcio_tipo_orden3', 41),
(3, 'descripcio_tipo_orden4', 121),
(13, 'descripcio_tipo_orden5', 17);


INSERT INTO TIPO_ESTADO (descripcion, id)
VALUES ('descripcion_estado1', 3454),
('descripcion_estado2', 8374),
('descripcion_estado3', 13),
('descripcion_estado4', 2),
('descripcion_estado5', 78);

INSERT INTO EMPRESA (rut, razon_social, nombre_fantasia)
VALUES (205547981, 'razon_social1','nom_fantasia1'),
(524541564, 'razon_social2','nom_fantasia2'),
(65234423, 'razon_social3','nom_fantasia3'),
(879767687, 'razon_social4','nom_fantasia4'),
(164154421, 'razon_social5','nom_fantasia5');
(769071342, 'razon_social6','nom_fantasia6');
(763471395, 'razon_social8','nom_fantasia8');

INSERT INTO ACCION (codigo_accion, descripcion, fecha_emision, EMPRESA_rut)
VALUES (15, 'descripcion1', '2020-01-22 04:05:06', 205547981),
(176, 'descripcion2', '2019-03-18 18:25:06', 205547981),
(874, 'descripcion3', '2019-11-10 13:15:06', 879767687),
(3, 'descripcion4', '2019-09-10 18:15:36', 164154421),
(115, 'descripcion5', '2020-02-25 14:45:46', 164154421);

INSERT INTO DETALLE_ORDEN (id_detalle_orden, ORDEN_nro_orden, ACCION_codigo_accion, TIPO_ESTADO_id)
VALUES (321, 1, 15, 13),
(5421, 3, 3, 2),
(45548, 17, 115, 78),
(434, 1, 15, 78),
(1, 3, 115, 13);
