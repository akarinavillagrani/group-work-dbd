/******Inicio : Tipo_Usuario******/
create table Tipo_Usuario(
	Id int not null primary key
	, Descripcion varchar(25) not null
);
insert into Tipo_Usuario  values (1,'Persona Natural');
insert into Tipo_Usuario  values (2,'Persona Juridica');
select * from Tipo_Usuario;
/******Fin : Tipo_Usuario******/



/******Inicio : Tipo_Transaccion******/
create table Tipo_Transaccion(
	Id int not null primary key
	, Descripcion varchar(25) not null
);
insert into Tipo_Transaccion  values (1,'Abono');
insert into Tipo_Transaccion  values (2,'Cargo');
select * from Tipo_Transaccion;
/******Fin : Tipo_Transaccion******/



/******Inicio : Tipo_Cuenta******/
create table Tipo_Cuenta(
	Id int not null primary key
	, Descripcion varchar(20) not null
);
insert into Tipo_Cuenta  values (1,'Cuenta Corriente');
insert into Tipo_Cuenta  values (2,'Cuenta Vista');
select * from Tipo_Cuenta;
/******Fin : Tipo_Cuenta******/



/******Inicio : Tipo_Estado******/
create table Tipo_Estado(
	Id int not null primary key
	, Descripcion varchar(20) not null
);
insert into Tipo_Estado  values (1,'Pendiente');
insert into Tipo_Estado  values (2,'Finalizada');
insert into Tipo_Estado  values (2,'Rechazada');
select * from Tipo_Estado
/******Fin : Tipo_Estado******/



/******Inicio : Tipo_Usuario******/
create table Banco(
	Id int not null primary key
	, Descripcion varchar(25) not null,
	UNIQUE(Descripcion)
);
insert into Banco  values (1,'Banco Estado');
insert into Banco  values (2,'Banco Santander');
select * from Banco;
/******Fin : Banco******/




/******Inicio : Empresa******/
CREATE TABLE Empresa (
	Id serial not null PRIMARY key,
	Rut VARCHAR (11) NOT NULL,
	Razon_Social VARCHAR (250) NOT NULL,
	Nombre_Fantasia VARCHAR (250) NOT null,
	UNIQUE(Rut)
);

insert into Empresa(Rut,Razon_Social,Nombre_Fantasia) values ('9.862.200-2','LATAM AIRLINES GROUP S.A.','Latam Airlines Group SA');
select * from Empresa
/******Fin : Empresa******/



/******Inicio : Accion******/
CREATE TABLE Accion (
	Id serial not null PRIMARY key,
	IdEmpresa int  NOT NULL,
	Codigo_Accion VARCHAR (50) NOT NULL,
	Descripcion VARCHAR (25) NOT NULL,
	Fecha_Emision TIMESTAMP NOT NULL,
	CONSTRAINT fk_accion_Empresa FOREIGN KEY(IdEmpresa) REFERENCES Empresa(Id),
	UNIQUE(Codigo_Accion)
);

insert into Accion (IdEmpresa	,Codigo_Accion 						, 	Descripcion		, 	Fecha_Emision	)
			values (1			,'LTM_' || ' ' || cast(1 as varchar),'Acccion_LATAM'	,NOW()				);
insert into Accion (IdEmpresa	,Codigo_Accion 						, 	Descripcion		, 	Fecha_Emision	)
			values (1			,'LTM_' || ' ' || cast(1 as varchar),'Acccion_LATAM'	,NOW()				);
insert into Accion (IdEmpresa	,Codigo_Accion 						, 	Descripcion		, 	Fecha_Emision	)
			values (1			,'LTM_' || ' ' || cast(1 as varchar),'Acccion_LATAM'	,NOW()				);
insert into Accion (IdEmpresa	,Codigo_Accion 						, 	Descripcion		, 	Fecha_Emision	)
			values (1			,'LTM_' || ' ' || cast(1 as varchar),'Acccion_LATAM'	,NOW()				);
insert into Accion (IdEmpresa	,Codigo_Accion 						, 	Descripcion		, 	Fecha_Emision	)
			values (1			,'LTM_' || ' ' || cast(currval() as varchar),'Acccion_LATAM'	,NOW()				);

select * from Accion;
/******Fin : Empresa******/



/******Inicio : Nacionalidad******/
CREATE TABLE Nacionalidad (
	Id serial not null PRIMARY key,
	Descripcion VARCHAR (70) NOT NULL,
	UNIQUE(Descripcion)
);
insert into nacionalidad (Descripcion) values('Chile');
insert into nacionalidad (Descripcion) values('Peru');
insert into nacionalidad (Descripcion) values('Argentina');
select * from nacionalidad;
/******Fin : Nacionalidad******/


/******Inicio : usuario******/
CREATE TABLE usuario (
	Id serial PRIMARY key,
	Rut VARCHAR (11) not null,
	Nombre VARCHAR (25) NOT NULL,
	Apellido VARCHAR (25) NOT NULL,
	EMail VARCHAR (30) NOT NULL,
	Direccion VARCHAR (250),
	Telefono integer not NULL,
	Sexo VARCHAR (10) NOT NULL,
	Es_Activo BIT NOT NULL,
	Es_Verificado bit  NOT NULL,
	Id_Nacionalidad integer not null,
	Id_Tipo_Usuario integer not null,
	Fecha_Creacion_Usu TIMESTAMP NOT NULL,
	Codigo_Serie Varchar(50) not null,
	Carnet_Foto BIT(1000),
    UNIQUE(Rut),
   CONSTRAINT fk_Nacionalidad_Usuario FOREIGN KEY(Id_Nacionalidad) REFERENCES Nacionalidad(Id),
   CONSTRAINT fk_TipoUsuario_Usuario FOREIGN KEY(Id_Tipo_Usuario) REFERENCES Tipo_Usuario(Id)
);

INSERT into usuario 
(Rut,Nombre,Apellido,EMail,Direccion,Telefono,Sexo,Es_Activo,Es_Verificado,Id_Nacionalidad,Id_Tipo_Usuario,Fecha_Creacion_Usu ,Codigo_Serie,Carnet_Foto)
values
('15915915-9','Juan','Perez','jperez@jperez.cl',NULL,123456789,'Sexo',B'1'::bit(1),B'1'::bit(1),1,1,NOW(),'123',NULL);
INSERT into usuario 
(Rut,Nombre,Apellido,EMail,Direccion,Telefono,Sexo,Es_Activo,Es_Verificado,Id_Nacionalidad,Id_Tipo_Usuario,Fecha_Creacion_Usu ,Codigo_Serie,Carnet_Foto)
values
('6197280-3','Juan 2','Perez 2','jperez2@jperez.cl',NULL,123456789,'Sexo',B'1'::bit(1),B'1'::bit(1),1,1,NOW(),'123',NULL);
INSERT into usuario 
(Rut,Nombre,Apellido,EMail,Direccion,Telefono,Sexo,Es_Activo,Es_Verificado,Id_Nacionalidad,Id_Tipo_Usuario,Fecha_Creacion_Usu ,Codigo_Serie,Carnet_Foto)
values
('16934561-9','Juan 3','Perez 3','jperez3@jperez.cl',NULL,123456789,'Sexo',B'1'::bit(1),B'1'::bit(1),1,1,NOW(),'123',NULL);
INSERT into usuario 
(Rut,Nombre,Apellido,EMail,Direccion,Telefono,Sexo,Es_Activo,Es_Verificado,Id_Nacionalidad,Id_Tipo_Usuario,Fecha_Creacion_Usu ,Codigo_Serie,Carnet_Foto)
values
('9096178-0','Juan 4','Perez 4','jperez4@jperez.cl',NULL,123456789,'Sexo',B'1'::bit(1),B'1'::bit(1),1,1,NOW(),'123',NULL);
select * from usuario;
/******Fin : usuario******/


/******Inicio : Cuenta_Bancaria******/
CREATE TABLE Cuenta_Bancaria (
	Id serial not null PRIMARY key ,
	Nro_Cuenta VARCHAR (25) NOT NULL,
	Id_Banco integer NOT NULL,
	Id_Usuario integer NOT null,
	Id_Tipo_Cuenta integer NOT null,
	CONSTRAINT fk_Banco_Cuenta FOREIGN KEY(Id_Banco) REFERENCES Banco(Id),
	CONSTRAINT fk_Usuario_Cuenta FOREIGN KEY(Id_Usuario) REFERENCES Usuario(Id),
    CONSTRAINT fk_Tipo_Cuenta_Cuenta1 FOREIGN KEY(Id_Tipo_Cuenta) REFERENCES Tipo_Cuenta(Id)
);

insert into Cuenta_Bancaria(Nro_Cuenta,	Id_Banco,Id_Usuario,Id_Tipo_Cuenta) values ('9.862.200-2',1,1,1);
insert into Cuenta_Bancaria(Nro_Cuenta,	Id_Banco,Id_Usuario,Id_Tipo_Cuenta) values ('9.862.200',2,1,1);
insert into Cuenta_Bancaria(Nro_Cuenta,	Id_Banco,Id_Usuario,Id_Tipo_Cuenta) values ('9.862-2',1,3,2);
select * from Cuenta_Bancaria
/******Fin : Cuenta_Bancaria******/




/******Inicio : Cartola_Cuenta******/
CREATE TABLE Cartola_Cuenta (
	Id serial not null PRIMARY key ,
	Fecha_Transaccion TIMESTAMP NOT NULL,
	Monto integer NOT NULL,
	Id_Usuario integer NOT null,
	Id_Tipo_Transaccion integer NOT null,	
	CONSTRAINT fk_Cartola_Cuenta_Usuario FOREIGN KEY(Id_Usuario) REFERENCES Usuario(Id),
    CONSTRAINT fk_Cartola_Cuenta_Tipo_Transaccion FOREIGN KEY(Id_Tipo_Transaccion) REFERENCES Tipo_Transaccion(Id)
);
/******Fin : Tipo_Orden******/


/******Inicio : Tipo_Orden******/
create table Tipo_Orden(
	Id int not null primary key
	, Descripcion varchar(25) not null
);
insert into Tipo_Orden  values (1,'Compra');
insert into Tipo_Orden  values (2,'Venta');
select * from Tipo_Orden;
/******Fin : Tipo_Orden******/

/******Inicio : Contrato******/
CREATE TABLE Contrato (
	Id serial not null PRIMARY key ,
	Direccion VARCHAR (250) NOT NULL,
	Fecha_Contratacion TIMESTAMP NOT NULL,
	Fecha_Finalizacion TIMESTAMP NOT NULL,
	Esta_Firmado bit  NOT NULL,
	Terminos_Y_Condiciones VARCHAR (1000) NOT NULL,
	Comision integer NOT null,
	PDF BIT(1000),
	Id_Emisor_Contrato integer NOT null,
	CONSTRAINT fk_Usuario_Contrato FOREIGN KEY(Id_Emisor_Contrato) REFERENCES Usuario(Id)    
);
/******Fin : Contrato******/

/******Inicio : Factura******/
CREATE TABLE Factura (
	Id serial not null PRIMARY key ,
	Id_Usuario integer NOT null,
	Id_Contrato integer NOT null,
	Monto_Comision integer  NOT NULL,
	Fecha_Facturacion TIMESTAMP NOT NULL,
	Comision integer NOT null,	
	CONSTRAINT fk_Usuario_Factura FOREIGN KEY(Id_Usuario) REFERENCES Usuario(Id),
	CONSTRAINT fk_Contrato_Factura FOREIGN KEY(Id_Contrato) REFERENCES Contrato(Id)    
);
/******Fin : Factura******/



/******Inicio : Orden******/
CREATE TABLE Orden (
	Id serial not null PRIMARY key ,
	Precio_Accion integer NOT null,
	Fecha_Transaccion TIMESTAMP NOT NULL,
	Id_Usuario integer NOT null,	
	CONSTRAINT fk_Orden_Factura FOREIGN KEY(Id_Usuario) REFERENCES Usuario(Id)    
);
/******Fin : Orden******/


/******Inicio : Detalle_Orden******/
CREATE TABLE Detalle_Orden (
	Id serial not null PRIMARY key ,
	Id_Orden integer NOT null,	
	Id_Accion integer NOT null,
	Id_Tipo_Estado int NOT null,
	CONSTRAINT fk_Detalle_Orden_Orden FOREIGN KEY(Id_Orden) REFERENCES Orden(Id),
	CONSTRAINT fk_Detalle_Orden_Accion FOREIGN KEY(Id_Accion) REFERENCES Accion(Id) ,   
	CONSTRAINT fk_Detalle_Orden_Estado FOREIGN KEY(Id_Tipo_Estado) REFERENCES Tipo_Estado(Id)    
);
/******Fin : Detalle_Orden******/