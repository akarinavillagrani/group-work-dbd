CREATE TABLE TIPO_USUARIO(
	id int NOT NULL,
	descripcion varchar(25) not null,
	PRIMARY KEY (id)
);

CREATE TABLE USUARIO(
	rut varchar(11) not null,
	nombre varchar(25) not null,
	apellido varchar(25) not null,
	email varchar(30) not null,
	direccion varchar(30) not null,
	telefono int not null,
	sexo varchar(10),
	es_activo bool not null,
	es_verificado bool not null,
	nacionalidad varchar(30) not null,
	fecha_creacion_usu timestamp not null,
	nro_serie varchar(50) not null,
	carnet_foto bit(1000) not null,
	tipo_usuario int not null,
	PRIMARY KEY (rut),
	CONSTRAINT FK_tipo_usuario_USUARIO FOREIGN KEY (tipo_usuario) REFERENCES TIPO_USUARIO(id),
	UNIQUE(email)
);

CREATE TABLE TIPO_TRANSACCION(
	id int not null, 
	descripcion varchar(25) not null,
	PRIMARY KEY (id)
);

CREATE TABLE CARTOLA_CUENTA(
	nro_operacion int not null,
	fecha_transaccion timestamp not null,
	monto int not null,
	USUARIO_rut varchar(11) not null,
	TIPO_TRANSACCION_id int not null,	
	PRIMARY KEY (nro_operacion),
	CONSTRAINT FK_USUARIO_rut_CARTOLA_CUENTA FOREIGN KEY (USUARIO_rut) REFERENCES USUARIO(rut),
	CONSTRAINT FK_TIPO_TRANSACCION_id_CARTOLA_CUENTA FOREIGN KEY (TIPO_TRANSACCION_id) REFERENCES TIPO_TRANSACCION(id)
);

CREATE TABLE TIPO_CUENTA(
	id int not null,
	descripcion varchar(20) not null,
	PRIMARY key (id)
);

CREATE TABLE CUENTA_BANCARIA(
	nro_cuenta varchar(20) not null,
	banco varchar(50) not null,
	USUARIO_rut varchar(11) not null,
	TIPO_CUENTA_id int not null,
	PRIMARY key (nro_cuenta),
	CONSTRAINT FK_USUARIO_rut_CUENTA_BANCARIA FOREIGN KEY (USUARIO_rut) REFERENCES USUARIO(rut),
	CONSTRAINT FK_TIPO_CUENTA_id_CUENTA_BANCARIA FOREIGN KEY (TIPO_CUENTA_id) REFERENCES TIPO_CUENTA(id)
);

CREATE TABLE CONTRATO(
	direccion varchar(20) not null PRIMARY KEY,
	fecha_contratacion timestamp not null,
	fecha_finalizacion timestamp not null,
	esta_firmado bool not null,
	terminos_y_condiciones varchar(1000),
	comision int not null,
	pdf bit(1000) not null,
	rut_emisor_contrato varchar(11) not null,
	CONSTRAINT FK_rut_emisor_contrato_CONTRATO FOREIGN KEY (rut_emisor_contrato) REFERENCES USUARIO(rut)
);

CREATE TABLE FACTURA(
	nro_factura int not null,
	monto_comision int not null,
	fecha_facturacion timestamp not null,
	USUARIO_rut varchar(11) not null,
	PRIMARY key (nro_factura),
	CONSTRAINT FK_USUARIO_rut_FACTURA FOREIGN KEY (USUARIO_rut) REFERENCES USUARIO(rut)
);

CREATE TABLE ORDEN(
	nro_orden int not null,
	precio_accion int not null,
	fecha_transaccion timestamp not null,
	USUARIO_rut varchar(11) not null,
	PRIMARY key (nro_orden),
	CONSTRAINT FK_USUARIO_rut_DETALLE_ORDEN FOREIGN KEY (USUARIO_rut) REFERENCES USUARIO(rut)
);

CREATE TABLE TIPO_ORDEN(
	id int not null,
	descripcion varchar(50) not null,
	ORDEN_nro_orden int not null,
	PRIMARY key (id),
	CONSTRAINT FK_ORDEN_nro_orden_TIPO_ORDEN FOREIGN KEY (ORDEN_nro_orden) REFERENCES ORDEN(nro_orden)
);

CREATE TABLE TIPO_ESTADO(
	id int not null,
	descripcion varchar(50) not null,
	PRIMARY key (id)
);

CREATE TABLE EMPRESA(
	rut int not null,
	razon_social varchar(250) not null,
	nombre_fantasia varchar(250) not null,
	PRIMARY key (rut)
);

CREATE TABLE ACCION(
	codigo_accion int not null,
	descripcion varchar(25) not null,
	fecha_emision timestamp not null,
	EMPRESA_rut int not null,
	PRIMARY key (codigo_accion),
	CONSTRAINT FK_EMPRESA_rut_ACCION FOREIGN KEY (EMPRESA_rut) REFERENCES EMPRESA(rut)
);

CREATE TABLE DETALLE_ORDEN(
	id_detalle_orden int not null,
	ORDEN_nro_orden int not null,
	ACCION_codigo_accion int not null,
	TIPO_ESTADO_id int not null,
	PRIMARY key (id_detalle_orden),
	CONSTRAINT FK_ORDEN_nro_orden_DETALLE_ORDEN FOREIGN KEY (ORDEN_nro_orden) REFERENCES ORDEN(nro_orden),
	CONSTRAINT FK_ACCION_codigo_accion_DETALLE_ORDEN FOREIGN KEY (ACCION_codigo_accion) REFERENCES ACCION(codigo_accion),
	CONSTRAINT FK_TIPO_ESTADO_id_DETALLE_ORDEN FOREIGN KEY (TIPO_ESTADO_id) REFERENCES TIPO_ESTADO(id)
);
